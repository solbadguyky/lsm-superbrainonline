<?php
	$CI = get_instance();
	$CI->load->database();
	$CI->load->dbforge();

	// insert col
	$CI->db->query('ALTER TABLE `lesson` ADD `external_url` IF NOT EXISTS VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `video_url`;');
?>
