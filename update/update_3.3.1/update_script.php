<?php
	$CI = get_instance();
	$CI->load->database();
	$CI->load->dbforge();

	// insert lesson col
	$CI->db->query('ALTER TABLE `lesson` ADD `external_url` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `video_url`;');

	// Add Course Position & buy url
	$CI->db->query('ALTER TABLE `course` ADD `position` INT NOT NULL AFTER `is_free_course`, ADD `buy_url` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `position`;');

	// Add Course seo-url
	$CI->db->query('ALTER TABLE `course` ADD `seo_url` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `position`;');

	//insert data in settings table
	$settings_data = array( 'value' => '3.3.1 - Đã tùy chỉnh bởi Superbrain' );
	$CI->db->where('key', 'version');
	$CI->db->update('settings', $settings_data);
?>
