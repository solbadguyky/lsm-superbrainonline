-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Máy chủ: localhost:3306
-- Thời gian đã tạo: Th4 25, 2020 lúc 04:33 PM
-- Phiên bản máy phục vụ: 10.3.22-MariaDB-log-cll-lve
-- Phiên bản PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `gsztxbjlhosting_01`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `addons`
--

CREATE TABLE `addons` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `unique_identifier` varchar(255) NOT NULL,
  `version` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `about` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `category`
--

CREATE TABLE `category` (
  `id` int(11) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent` int(11) DEFAULT 0,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` int(11) DEFAULT NULL,
  `last_modified` int(11) DEFAULT NULL,
  `font_awesome_class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `category`
--

INSERT INTO `category` (`id`, `code`, `name`, `parent`, `slug`, `date_added`, `last_modified`, `font_awesome_class`, `thumbnail`) VALUES
(1, '8cd963adf4', 'GIỚI THIỆU', 0, 'giỚi-thiỆu', 1584378000, 1584378000, 'fas fa-address-book', 'a2fad6ed12a4295f0e72af52a0961c9c.jpg'),
(2, '82ed584024', 'Lợi ích - Phương châm', 1, 'lợi-ích-phương-châm', 1584378000, 1584378000, NULL, NULL),
(3, 'f5f58c2ae9', 'Lộ trình học', 1, 'lộ-trình-học', 1584378000, NULL, NULL, NULL),
(4, '9ea8af9845', 'Toán Trí Tuệ FingerMath', 0, 'toán-trí-tuệ-fingermath', 1584378000, 1584378000, 'fas fa-graduation-cap', 'category-thumbnail.png'),
(5, 'ab8df70f83', 'Câu chuyện Superbrain', 1, 'câu-chuyện-superbrain', 1584378000, NULL, NULL, NULL),
(6, '230b28a621', 'Phương pháp FingerMath', 4, 'phương-pháp-fingermath', 1584378000, NULL, NULL, NULL),
(7, '2ca8e3c7ab', 'Các khóa học FingerMath', 4, 'các-khóa-học-fingermath', 1584378000, NULL, NULL, NULL),
(8, 'c1adf798f5', 'GÓC CẢM NHẬN', 0, 'gÓc-cẢm-nhẬn', 1584378000, NULL, 'far fa-comment', 'category-thumbnail.png'),
(9, '35477e44a0', 'Cảm nhận phụ huynh', 8, 'cảm-nhận-phụ-huynh', 1584378000, NULL, NULL, NULL),
(10, 'cf441b4632', 'Cảm nhận học viên', 8, 'cảm-nhận-học-viên', 1584378000, NULL, NULL, NULL),
(11, '665e41d385', 'Cảm nhận giáo viên', 8, 'cảm-nhận-giáo-viên', 1584378000, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('b348c4285a6ea9c668d7df12e7ecc6356448f393', '137.226.113.27', 1587792164, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373739323136343b636172745f6974656d737c613a303a7b7d),
('90b3da01668ddfb6379ff71331ce83fd9c9ee3eb', '27.68.1.212', 1587794003, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373739343030333b636172745f6974656d737c613a303a7b7d),
('7628019182ef2eda5d1bf2bd8db7b7d28309c4dc', '27.68.1.212', 1587794538, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373739343533383b636172745f6974656d737c613a303a7b7d757365725f69647c733a323a223536223b726f6c655f69647c733a313a2232223b726f6c657c733a343a2255736572223b6e616d657c733a31373a224b6875c3aa2048e1bb93205669e1bb8774223b757365725f6c6f67696e7c733a313a2231223b),
('cc938e36160dfd499772dcf800ac3a2fa664156b', '27.68.1.212', 1587794003, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373739343030333b636172745f6974656d737c613a303a7b7d),
('ba820f7403894c0fc614a24fe024a84fe1bc7c41', '115.79.214.137', 1587794228, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373739343138323b636172745f6974656d737c613a303a7b7d),
('4a0d35dc4801354ff0889b494a9d7e1e90fc5999', '113.161.57.30', 1587794802, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373739343533363b636172745f6974656d737c613a303a7b7d757365725f69647c733a323a223634223b726f6c655f69647c733a313a2231223b726f6c657c733a353a2241646d696e223b6e616d657c733a31343a22446576205375706572627261696e223b61646d696e5f6c6f67696e7c733a313a2231223b),
('808d4571f11689a4b90a9819b65de13c9e5298ff', '27.68.1.212', 1587795080, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373739353038303b636172745f6974656d737c613a303a7b7d757365725f69647c733a323a223536223b726f6c655f69647c733a313a2232223b726f6c657c733a343a2255736572223b6e616d657c733a31373a224b6875c3aa2048e1bb93205669e1bb8774223b757365725f6c6f67696e7c733a313a2231223b),
('732461f4e571f20c4d52bbcaafb32832fc20d3ff', '27.65.196.70', 1587794758, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373739343735383b636172745f6974656d737c613a303a7b7d),
('0d8b10aef147052cd6a245844ef92efe869d6963', '171.254.81.98', 1587795097, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373739353039373b636172745f6974656d737c613a303a7b7d757365725f69647c733a323a223634223b726f6c655f69647c733a313a2231223b726f6c657c733a353a2241646d696e223b6e616d657c733a31343a22446576205375706572627261696e223b61646d696e5f6c6f67696e7c733a313a2231223b),
('624eea0c367bdf95e6826995cb74ad77efe2a864', '27.68.1.212', 1587795393, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373739353339333b636172745f6974656d737c613a303a7b7d757365725f69647c733a323a223536223b726f6c655f69647c733a313a2232223b726f6c657c733a343a2255736572223b6e616d657c733a31373a224b6875c3aa2048e1bb93205669e1bb8774223b757365725f6c6f67696e7c733a313a2231223b6c61796f75747c733a343a226c697374223b),
('33493e7f6ad2b0778597026dc4bb75f384fe374d', '171.254.81.98', 1587795162, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373739353039373b636172745f6974656d737c613a303a7b7d757365725f69647c733a323a223634223b726f6c655f69647c733a313a2231223b726f6c657c733a353a2241646d696e223b6e616d657c733a31343a22446576205375706572627261696e223b61646d696e5f6c6f67696e7c733a313a2231223b),
('3bfd70befea2794d076464663a3ee1d69c00897a', '14.245.9.9', 1587796300, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373739353330313b636172745f6974656d737c613a303a7b7d757365725f69647c733a323a223632223b726f6c655f69647c733a313a2232223b726f6c657c733a343a2255736572223b6e616d657c733a31353a224b686f61204d6169205469e1babf6e223b757365725f6c6f67696e7c733a313a2231223b),
('7894d91d950351f8e4d6519d1fc5925e0ecabd21', '27.68.1.212', 1587795854, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373739353835343b636172745f6974656d737c613a303a7b7d6c61796f75747c733a343a226c697374223b757365725f69647c733a323a223536223b726f6c655f69647c733a313a2232223b726f6c657c733a343a2255736572223b6e616d657c733a31373a224b6875c3aa2048e1bb93205669e1bb8774223b757365725f6c6f67696e7c733a313a2231223b),
('14d98256cb1fc3c71880d9031641afd935c59f2a', '27.68.1.212', 1587801162, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373830313136323b636172745f6974656d737c613a303a7b7d6c61796f75747c733a343a226c697374223b),
('42df35a013275f915e05d51df1f14e07a19e9f34', '115.79.214.137', 1587798689, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373739383638393b636172745f6974656d737c613a303a7b7d757365725f69647c733a313a2236223b726f6c655f69647c733a313a2231223b726f6c657c733a353a2241646d696e223b6e616d657c733a31353a2248e1baa16e68204e677579e1bb856e223b61646d696e5f6c6f67696e7c733a313a2231223b),
('ad39b90758c2c867dba49d5ad69ec3ac1cfa7f3b', '115.79.214.137', 1587797004, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373739373030343b636172745f6974656d737c613a303a7b7d757365725f69647c733a313a2233223b726f6c655f69647c733a313a2232223b726f6c657c733a343a2255736572223b6e616d657c733a31373a22546869c3aa6e20c3826e20486fc3a06e67223b757365725f6c6f67696e7c733a313a2231223b),
('59de37c563e58eab9ebace90c9ffa475dff37904', '115.79.214.137', 1587798695, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373739383639353b636172745f6974656d737c613a303a7b7d757365725f69647c733a313a2233223b726f6c655f69647c733a313a2232223b726f6c657c733a343a2255736572223b6e616d657c733a31373a22546869c3aa6e20c3826e20486fc3a06e67223b757365725f6c6f67696e7c733a313a2231223b),
('66f688323ed6829b3cc1f54d80086e87226b6992', '137.226.113.26', 1587797225, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373739373232353b636172745f6974656d737c613a303a7b7d),
('d5296c2e46a1734b63777eca47f2bd115c5186a9', '115.79.214.137', 1587798991, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373739383939313b636172745f6974656d737c613a303a7b7d757365725f69647c733a313a2236223b726f6c655f69647c733a313a2231223b726f6c657c733a353a2241646d696e223b6e616d657c733a31353a2248e1baa16e68204e677579e1bb856e223b61646d696e5f6c6f67696e7c733a313a2231223b),
('fe831ac1cb78540e02c91fa3cdb1ace7bc3a59cb', '115.79.214.137', 1587798999, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373739383939393b636172745f6974656d737c613a303a7b7d757365725f69647c733a313a2233223b726f6c655f69647c733a313a2232223b726f6c657c733a343a2255736572223b6e616d657c733a31373a22546869c3aa6e20c3826e20486fc3a06e67223b757365725f6c6f67696e7c733a313a2231223b),
('b587a407ad0628377e5e00d14f0bf1c548d08423', '115.79.214.137', 1587799586, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373739393538363b636172745f6974656d737c613a303a7b7d757365725f69647c733a313a2236223b726f6c655f69647c733a313a2231223b726f6c657c733a353a2241646d696e223b6e616d657c733a31353a2248e1baa16e68204e677579e1bb856e223b61646d696e5f6c6f67696e7c733a313a2231223b),
('6b0f53ccba286bd11c09354e01f4b8ad62406171', '115.79.214.137', 1587799011, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373739383939393b636172745f6974656d737c613a303a7b7d757365725f69647c733a313a2233223b726f6c655f69647c733a313a2232223b726f6c657c733a343a2255736572223b6e616d657c733a31373a22546869c3aa6e20c3826e20486fc3a06e67223b757365725f6c6f67696e7c733a313a2231223b),
('030064d25019cb656e1eb0ef8d90046a4cfae54c', '66.249.65.103', 1587799327, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373739393332373b636172745f6974656d737c613a303a7b7d),
('b43157c39ca491c9b606ae9fc297935bd1778440', '66.249.65.105', 1587799327, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373739393332373b636172745f6974656d737c613a303a7b7d),
('bc10e4f1ff35b13cd5939a2a7aa5d8b5d7189f5c', '115.79.214.137', 1587802331, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373830323333313b636172745f6974656d737c613a303a7b7d757365725f69647c733a313a2236223b726f6c655f69647c733a313a2231223b726f6c657c733a353a2241646d696e223b6e616d657c733a31353a2248e1baa16e68204e677579e1bb856e223b61646d696e5f6c6f67696e7c733a313a2231223b),
('8515e872cdabea3327efd2491a0dac10394a5a86', '171.253.137.249', 1587800232, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373830303232393b636172745f6974656d737c613a303a7b7d),
('46a9c2a5bef4ea0ec4a0764f96075ea2d8ccc035', '103.98.157.233', 1587800531, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373830303533313b636172745f6974656d737c613a303a7b7d),
('68bb4bd2e39b89c5d73de845cc09cfc27431e162', '66.249.65.105', 1587800560, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373830303536303b636172745f6974656d737c613a303a7b7d),
('05ce055d49532d8f93975a67e03c440daeac3d07', '27.68.1.212', 1587801341, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373830313136323b636172745f6974656d737c613a303a7b7d6c61796f75747c733a343a226c697374223b),
('5ae057a62a282aac2ceb0f454da31dce05c83aca', '14.187.67.195', 1587802906, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373830323930363b636172745f6974656d737c613a303a7b7d),
('fcc92bddfe8266e01632361bce373f0d99dc4035', '27.65.196.70', 1587802028, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373830323032383b636172745f6974656d737c613a303a7b7d),
('612dfb3f4065d6623d866935aba2ba9da6312677', '100.101.249.187', 1587802111, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373830323131313b636172745f6974656d737c613a303a7b7d),
('b84f9ef8330f74a7c0764b102cb0673a4a755eab', '115.79.214.137', 1587802360, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373830323333313b636172745f6974656d737c613a303a7b7d757365725f69647c733a313a2236223b726f6c655f69647c733a313a2231223b726f6c657c733a353a2241646d696e223b6e616d657c733a31353a2248e1baa16e68204e677579e1bb856e223b61646d696e5f6c6f67696e7c733a313a2231223b666c6173685f6d6573736167657c733a34333a2244e1bbaf206c69e1bb877520c491c3a320c491c6b0e1bba3632078c3b361207468c3a06e682063c3b46e67223b5f5f63695f766172737c613a313a7b733a31333a22666c6173685f6d657373616765223b733a333a226f6c64223b7d),
('6959ae7ced87d956876d4aadd6ce8c7f160f07af', '14.233.2.219', 1587802598, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373830323539383b636172745f6974656d737c613a303a7b7d),
('a2079b588d5626a237f2360cd390b772b5de124e', '14.187.67.195', 1587803216, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373830333231363b636172745f6974656d737c613a303a7b7d6c61796f75747c733a343a226c697374223b),
('7f82d3c169a0a5d54cc30803c25eda7a53b33084', '115.79.214.137', 1587803156, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373830333135353b636172745f6974656d737c613a303a7b7d),
('82a29c08c6646d684ad3767f19a44b8433d85df6', '14.187.67.195', 1587803851, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373830333835313b636172745f6974656d737c613a303a7b7d6c61796f75747c733a343a226c697374223b),
('24bf897742201c0191598be298b6b69f199fc010', '115.79.214.137', 1587803567, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373830333536323b636172745f6974656d737c613a303a7b7d),
('ab3c4b704c6ce87d5b2c1f93982d5674dfc3f476', '14.187.67.195', 1587804185, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373830343138353b636172745f6974656d737c613a303a7b7d6c61796f75747c733a343a226c697374223b),
('80542cae648b35eeaccb0c81d5cbb7a463946fcd', '14.187.67.195', 1587804945, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373830343934353b636172745f6974656d737c613a303a7b7d6c61796f75747c733a343a226c697374223b746f74616c5f70726963655f6f665f636865636b696e675f6f75747c693a303b),
('170127b9cca6da264108b35642cca102e3ae3927', '14.187.67.195', 1587805087, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373830343934353b636172745f6974656d737c613a303a7b7d6c61796f75747c733a343a226c697374223b746f74616c5f70726963655f6f665f636865636b696e675f6f75747c693a303b),
('77fde88053754edf9ed2a2bd7f2f0eb3434eb99b', '27.65.196.70', 1587805686, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373830353638363b636172745f6974656d737c613a303a7b7d),
('2eaf2cd31b0ab72f9eccc2543708aadfbc34d122', '173.252.83.8', 1587805987, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373830353938373b636172745f6974656d737c613a303a7b7d),
('8f5a99c80353655510011f2d7df03a2141ead636', '171.254.81.98', 1587806458, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373830363333323b636172745f6974656d737c613a303a7b7d757365725f69647c733a313a2233223b726f6c655f69647c733a313a2232223b726f6c657c733a343a2255736572223b6e616d657c733a31373a22546869c3aa6e20c3826e20486fc3a06e67223b666c6173685f6d6573736167657c733a33303a224368c3a06f206de1bbab6e6720546869c3aa6e20c3826e20486fc3a06e67223b5f5f63695f766172737c613a313a7b733a31333a22666c6173685f6d657373616765223b733a333a226f6c64223b7d757365725f6c6f67696e7c733a313a2231223b),
('9000859b02bb99944c21fd5f637c920d2e115048', '171.254.81.98', 1587806387, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373830363335303b636172745f6974656d737c613a303a7b7d757365725f69647c733a323a223634223b726f6c655f69647c733a313a2231223b726f6c657c733a353a2241646d696e223b6e616d657c733a31343a22446576205375706572627261696e223b666c6173685f6d6573736167657c733a32373a224368c3a06f206de1bbab6e6720446576205375706572627261696e223b5f5f63695f766172737c613a313a7b733a31333a22666c6173685f6d657373616765223b733a333a226f6c64223b7d61646d696e5f6c6f67696e7c733a313a2231223b),
('622e859502f41e78b4edbd0a5e6ed94e4d05e08b', '115.79.214.137', 1587807074, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538373830363932353b636172745f6974656d737c613a303a7b7d757365725f69647c733a323a223131223b726f6c655f69647c733a313a2232223b726f6c657c733a343a2255736572223b6e616d657c733a393a22686f616e6720626f6e223b757365725f6c6f67696e7c733a313a2231223b);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `comment`
--

CREATE TABLE `comment` (
  `id` int(11) UNSIGNED NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `commentable_id` int(11) DEFAULT NULL,
  `commentable_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` int(11) DEFAULT NULL,
  `last_modified` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `course`
--

CREATE TABLE `course` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_description` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `outcomes` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sub_category_id` int(11) DEFAULT NULL,
  `section` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `requirements` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` double DEFAULT NULL,
  `discount_flag` int(11) DEFAULT 0,
  `discounted_price` int(11) DEFAULT NULL,
  `level` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` int(11) DEFAULT NULL,
  `last_modified` int(11) DEFAULT NULL,
  `visibility` int(11) DEFAULT NULL,
  `is_top_course` int(11) DEFAULT 0,
  `is_admin` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `course_overview_provider` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_free_course` int(11) DEFAULT NULL,
  `position` int(11) NOT NULL,
  `seo_url` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `buy_url` varchar(250) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `course`
--

INSERT INTO `course` (`id`, `title`, `short_description`, `description`, `outcomes`, `language`, `category_id`, `sub_category_id`, `section`, `requirements`, `price`, `discount_flag`, `discounted_price`, `level`, `user_id`, `thumbnail`, `video_url`, `date_added`, `last_modified`, `visibility`, `is_top_course`, `is_admin`, `status`, `course_overview_provider`, `meta_keywords`, `meta_description`, `is_free_course`, `position`, `seo_url`, `buy_url`) VALUES
(1, 'Khóa học Finger Online 1', '24 Video bài học & kỹ năng​\r\n<br>Phần mềm luyện tập Online độc quyền\r\n<br>12 buổi buổi học tương tác trực tuyến cùng giáo viên​\r\n<br>500 bài tập rèn luyện​\r\n<br>Báo cáo kết quả mỗi tuần​\r\n<br>Cam kết sự tiến bộ của trẻ​', '<p>Độ tuổi từ 6 – 12 tuổi là “Thời điểm vàng”. Đây là giai đoạn phát triển mạnh mẽ nhất của trẻ về cả thể chất lẫn tinh thần. Những kiến thức, trải nghiệm ở giai đoạn này là nền tảng vững chắc cho trẻ trong những năm học kế tiếp. Đến với khóa học, tham gia trẻ sẽ được tiếp xúc với phương pháp mới – phương pháp toán trí tuệ. Ngoài ra, trẻ còn được tham gia các lớp học trực tuyến với giáo viên cùng nhiều chủ đề hấp dẫn như kỹ năng, nghiên cứu khoa học, hội họa... Và hơn thế nữa, khóa học mang lại cho trẻ một hành trang vững chắc trong tương lai.<br></p><p>Phương pháp Superbrain là phương pháp luyện tập. Chính vì thế, lợi thế của gói 3 tháng dành cho con đó là:<br>- 100 ngày trải nghiệm phần mềm Online độc quyền.<br>- 500+ bài tập luyện tập được gửi về liên tục hàng ngày.<br>- 24 bộ video học phương pháp FingerMath đến từ Hàn Quốc.<br>- 12 buổi học trực tuyến cùng giáo viên hướng dẫn Superbrain với nhiều chủ đề hấp dẫn.<br>- Đồng hành cùng Superbrain trong các cuộc thi Toán trí tuệ trong và ngoài nước.<br>- Giấy chứng nhận kết thúc khóa từ Superbrain Việt Nam.<br>- Tích điểm Power và đổi các phần quà hấp dẫn từ Superbrain.<br>- Quy đổi khóa học Online thành khóa học trực tiếp tại các cơ sở Superbrain trên toàn quốc.<br>- Giáo viên chăm sóc trực, tương tác tiếp với trẻ.<br>Khóa học sẽ tạo cho con nền tảng trong quá trình kết nối cơ thể với tư duy, phát triển cân bằng hai bán cầu não để xây dựng sự tập trung, ghi nhớ, phản xạ và tự tin.</p>', '[\"1. Kh\\u1ea3 n\\u0103ng t\\u00ednh to\\u00e1n linh ho\\u1ea1t, ch\\u00ednh x\\u00e1c t\\u1eeb 0 \\u2013 99 ch\\u1ec9 v\\u1edbi \\u0111\\u00f4i b\\u00e0n tay, gi\\u00fap k\\u00edch th\\u00edch t\\u1ed1i \\u0111a s\\u1ef1 ph\\u1ed1i h\\u1ee3p nh\\u1ecbp nh\\u00e0ng gi\\u1eefa ho\\u1ea1t \\u0111\\u1ed9ng c\\u01a1 th\\u1ec3 v\\u1edbi t\\u01b0 duy. \",\"2. Kh\\u1ea3 n\\u0103ng gi\\u1ea3i quy\\u1ebft ph\\u00e9p t\\u00ednh v\\u1edbi c\\u00e1c d\\u00e3y s\\u1ed1 li\\u00ean t\\u1ee5c, t\\u1eeb \\u0111\\u00f3 t\\u0103ng kh\\u1ea3 n\\u0103ng t\\u1eadp trung v\\u00e0 ghi nh\\u1edb. \",\"3. Kh\\u1ea3 n\\u0103ng ho\\u00e0n th\\u00e0nh ch\\u00ednh x\\u00e1c k\\u1ebft qu\\u1ea3 d\\u00e3y t\\u00ednh d\\u00e0i ch\\u1ec9 trong v\\u00e0i gi\\u00e2y, t\\u1eeb \\u0111\\u00f3 ph\\u00e1t huy t\\u1ed1i \\u01b0u s\\u1ef1 nh\\u1ea1y b\\u00e9n v\\u00e0 x\\u1eed l\\u00ed t\\u00ecnh hu\\u1ed1ng. \",\"4. Kh\\u1ea3 n\\u0103ng t\\u01b0 duy v\\u00e0 t\\u01b0\\u1edfng t\\u01b0\\u1ee3ng \\u0111\\u01b0\\u1ee3c ph\\u00e1t huy t\\u1ed1i \\u01b0u qua vi\\u1ec7c r\\u00e8n luy\\u1ec7n c\\u00e1c b\\u00e0i to\\u00e1n v\\u1edbi d\\u00e3y s\\u1ed1 d\\u00e0i.\"]', 'vietnamese', 1, 3, '[2,3,4,5,6,7,8,9,10,11,12,13,28]', '[\"Gi\\u00fap tr\\u1ebb t\\u0103ng kh\\u1ea3 n\\u0103ng t\\u1eadp trung cao\",\"Gi\\u00fap tr\\u1ebb t\\u0103ng kh\\u1ea3 n\\u0103ng ghi nh\\u1edb ch\\u00ednh x\\u00e1c c\\u00e1c th\\u00f4ng tin\",\"Gi\\u00fap tr\\u1ebb duy tr\\u00ec v\\u00e0 t\\u1ea1o th\\u00f3i quen h\\u1ecdc t\\u1eadp\",\"Gi\\u00fap tr\\u1ebb t\\u0103ng ph\\u1ea3n x\\u1ea1 v\\u00e0 t\\u1ed1c d\\u1ed9 t\\u00ednh to\\u00e1n ch\\u00ednh x\\u00e1c c\\u00e1c con s\\u1ed1\",\"Gi\\u00fap tr\\u1ebb ph\\u00e1t tri\\u1ec3n \\u0111\\u1ed3ng \\u0111\\u1ec1u c\\u1ea3 2 b\\u00e1n \\u0111\\u1ea7u n\\u00e3o v\\u1edbi s\\u1ef1 k\\u1ebft h\\u1ee3p gi\\u1eefa c\\u01a1 th\\u1ec3 v\\u00e0 t\\u01b0 duy.  T\\u1eeb \\u0111\\u00f3 tr\\u1ebb \\u0111\\u01b0\\u1ee3c kh\\u01a1i d\\u1eady v\\u00e0 ph\\u00e1t  huy ti\\u1ec1m n\\u0103ng t\\u1ed1i \\u0111a c\\u1ee7a b\\u1ea3n th\\u00e2n.\",\"Gi\\u00fap tr\\u1ebb t\\u1ef1 tin th\\u1ec3 hi\\u1ec7n b\\u1ea3n th\\u00e2n m\\u00ecnh\"]', 3000000, 1, 1950000, 'beginner', 1, NULL, 'https://youtu.be/z6nfBvrNyOA', 1584378000, 1587747600, NULL, 1, 1, 'active', 'youtube', '', 'Khóa học FingerMath online', NULL, 2, '', 'https://superbrain.edu.vn/san-pham/finger-online-1'),
(2, 'Khóa học Finger Online 2', '24 Video bài học & kỹ năng​\r\n<br>Phần mềm luyện tập Online độc quyền\r\n<br>12 buổi buổi học tương tác trực tuyến cùng giáo viên​\r\n<br>500 bài tập rèn luyện​\r\n<br>Báo cáo kết quả mỗi tuần​\r\n<br>Cam kết sự tiến bộ của trẻ​', '<p>Độ tuổi từ 6 – 12 tuổi là “Thời điểm vàng”. Đây là giai đoạn phát triển mạnh mẽ nhất của trẻ về cả thể chất lẫn tinh thần. Những kiến thức, trải nghiệm ở giai đoạn này là nền tảng vững chắc cho trẻ trong những năm học kế tiếp. Đến với khóa học, tham gia trẻ sẽ được tiếp xúc với phương pháp mới – phương pháp toán trí tuệ. Ngoài ra, trẻ còn được tham gia các lớp học trực tuyến với giáo viên cùng nhiều chủ đề hấp dẫn như kỹ năng, nghiên cứu khoa học, hội họa... Và hơn thế nữa, khóa học mang lại cho trẻ một hành trang vững chắc trong tương lai.<br></p><p>Phương pháp Superbrain là phương pháp luyện tập. Chính vì thế, lợi thế của khóa học Finger Online 2 dành cho con đó là:</p><p>- Tiếp tục rèn luyện các kỹ năng Tập trung - ghi nhớ - phản xạ tự tin. Đồng thời hoàn thiện kỹ năng tính toán các bài toán từ 0 - 99.<br>- 100+ ngày trải nghiệm phần mềm Online độc quyền.<br>- 500+ bài tập luyện tập được gửi về hàng ngày.<br>- 24 Video học phương pháp FingerMath đến từ Hàn Quốc giúp con tròn vẹn kĩ năng tính toán.<br>- 12 buổi học trực tuyến cùng giáo viên hướng dẫn Superbrain với nhiều chủ đề hấp dẫn.<br>- Tích điểm Power và đổi các phần quà hấp dẫn từ Superbrain.<br>- Giấy chứng nhận kết thúc khóa từ Superbrain Việt Nam.<br>- Đồng hành cùng Superbrain trong các cuộc thi Toán trí tuệ trong và ngoài nước.<br>- Quy đổi khóa học Online thành khóa học trực tiếp tại các cơ sở Superbrain trên toàn quốc.<br>- Giáo viên chăm sóc, tương tác trực tiếp với con.</p><p>Với phương pháp luyện tập là cốt lõi, nếu quá trình luyện tập càng dài thì kĩ năng tính toán, khả năng tập trung, ghi nhớ, phản xạ, tự tin càng vượt trội. Cũng giống như thể thao, luyện tập càng lâu dài, cơ thể càng săn chắc. Superbrain luyện tập càng lâu dài, bộ não càng khỏe mạnh.</p>', '[\"1. Kh\\u1ea3 n\\u0103ng t\\u00ednh to\\u00e1n linh ho\\u1ea1t, ch\\u00ednh x\\u00e1c t\\u1eeb 0 \\u2013 99 ch\\u1ec9 v\\u1edbi \\u0111\\u00f4i b\\u00e0n tay, gi\\u00fap k\\u00edch th\\u00edch t\\u1ed1i \\u0111a s\\u1ef1 ph\\u1ed1i h\\u1ee3p nh\\u1ecbp nh\\u00e0ng gi\\u1eefa ho\\u1ea1t \\u0111\\u1ed9ng c\\u01a1 th\\u1ec3 v\\u1edbi t\\u01b0 duy. \",\"2. Kh\\u1ea3 n\\u0103ng gi\\u1ea3i quy\\u1ebft ph\\u00e9p t\\u00ednh v\\u1edbi c\\u00e1c d\\u00e3y s\\u1ed1 li\\u00ean t\\u1ee5c, t\\u1eeb \\u0111\\u00f3 t\\u0103ng kh\\u1ea3 n\\u0103ng t\\u1eadp trung v\\u00e0 ghi nh\\u1edb. \",\"3. Kh\\u1ea3 n\\u0103ng ho\\u00e0n th\\u00e0nh ch\\u00ednh x\\u00e1c k\\u1ebft qu\\u1ea3 d\\u00e3y t\\u00ednh d\\u00e0i ch\\u1ec9 trong v\\u00e0i gi\\u00e2y, t\\u1eeb \\u0111\\u00f3 ph\\u00e1t huy t\\u1ed1i \\u01b0u s\\u1ef1 nh\\u1ea1y b\\u00e9n v\\u00e0 x\\u1eed l\\u00ed t\\u00ecnh hu\\u1ed1ng. \",\"4. Kh\\u1ea3 n\\u0103ng t\\u01b0 duy v\\u00e0 t\\u01b0\\u1edfng t\\u01b0\\u1ee3ng \\u0111\\u01b0\\u1ee3c ph\\u00e1t huy t\\u1ed1i \\u01b0u qua vi\\u1ec7c r\\u00e8n luy\\u1ec7n c\\u00e1c b\\u00e0i to\\u00e1n v\\u1edbi d\\u00e3y s\\u1ed1 d\\u00e0i.\"]', 'vietnamese', 1, 3, '[29,30,31,32,33,34,35,36,37,38,39,40]', '[\"Gi\\u00fap tr\\u1ebb t\\u0103ng kh\\u1ea3 n\\u0103ng t\\u1eadp trung cao\",\"Gi\\u00fap tr\\u1ebb t\\u0103ng kh\\u1ea3 n\\u0103ng ghi nh\\u1edb ch\\u00ednh x\\u00e1c c\\u00e1c th\\u00f4ng tin\",\"Gi\\u00fap tr\\u1ebb duy tr\\u00ec v\\u00e0 t\\u1ea1o th\\u00f3i quen h\\u1ecdc t\\u1eadp\",\"Gi\\u00fap tr\\u1ebb t\\u0103ng ph\\u1ea3n x\\u1ea1 v\\u00e0 t\\u1ed1c d\\u1ed9 t\\u00ednh to\\u00e1n ch\\u00ednh x\\u00e1c c\\u00e1c con s\\u1ed1\",\"Gi\\u00fap tr\\u1ebb ph\\u00e1t tri\\u1ec3n \\u0111\\u1ed3ng \\u0111\\u1ec1u c\\u1ea3 2 b\\u00e1n \\u0111\\u1ea7u n\\u00e3o v\\u1edbi s\\u1ef1 k\\u1ebft h\\u1ee3p gi\\u1eefa c\\u01a1 th\\u1ec3 v\\u00e0 t\\u01b0 duy.  T\\u1eeb \\u0111\\u00f3 tr\\u1ebb \\u0111\\u01b0\\u1ee3c kh\\u01a1i d\\u1eady v\\u00e0 ph\\u00e1t  huy ti\\u1ec1m n\\u0103ng t\\u1ed1i \\u0111a c\\u1ee7a b\\u1ea3n th\\u00e2n.\",\"Gi\\u00fap tr\\u1ebb t\\u1ef1 tin th\\u1ec3 hi\\u1ec7n b\\u1ea3n th\\u00e2n m\\u00ecnh\"]', 3000000, 1, 1950000, 'beginner', 1, NULL, 'https://youtu.be/z6nfBvrNyOA', 1584723600, 1587142800, NULL, 1, 1, 'active', 'youtube', '', '', NULL, 3, '', 'https://superbrain.edu.vn/san-pham/finger-online-2'),
(3, 'Finger Online 1 tuần', 'Video bài học sinh động\r\n<br>Phần mềm luyện tập Superbrain online\r\n<br>Lớp học trực tuyến\r\n<br>Kho bài tập luyện tập mỗi ngày\r\n<br>Báo cáo tiến bộ của học viên', '<p>Độ tuổi từ 4 – 12 tuổi là “Thời điểm vàng”. Đây là giai đoạn phát triển mạnh mẽ nhất của trẻ về cả thể chất lẫn tinh thần. Những kiến thức, trải nghiệm ở giai đoạn này là nền tảng vững chắc cho trẻ trong những năm học kế tiếp. Đến với khóa học, tham gia trẻ sẽ được tiếp xúc với phương pháp mới – phương pháp toán trí tuệ. Ngoài ra, trẻ còn được tham gia các lớp học trực tuyến với giáo viên cùng nhiều chủ đề hấp dẫn như kỹ năng, nghiên cứu khoa học, hội họa... Và hơn thế nữa, khóa học mang lại cho trẻ một hành trang vững chắc trong tương lai.</p><p>Superbrain dành tặng khóa học để ba mẹ cho con trải nghiệm phương pháp Toán trí tuệ Online trong thời gian 7 ngày. Với khóa học trải nghiệm này, con sẽ được:<br>- 1 Video học qua phương pháp FingerMath<br>- 1 Video kĩ năng sống chủ đề Em yêu khoa học<br>- 1 buổi học trực tuyến cùng giáo viên hướng dẫn Superbrain<br>- Trải nghiệm phần mềm Online độc quyền trong vòng 7 ngày<br>- 70 bài tập rèn luyện dành cho con<br>Khóa học này chỉ là khóa học trải nghiệm trong thời gian 1 tuần nên phụ huynh chỉ đăng ký mua 1 gói duy nhất.</p>', '[\"Kh\\u00e1m ph\\u00e1 s\\u1ef1 m\\u1edbi m\\u1ebb c\\u00f9ng ph\\u01b0\\u01a1ng ph\\u00e1p FingerMath\",\"X\\u00e2y d\\u1ef1ng ngu\\u1ed3n c\\u1ea3m h\\u1ee9ng h\\u1ecdc t\\u1eadp cho con tr\\u1ebb\",\"\\u0110\\u1ecbnh h\\u01b0\\u1edbng ban \\u0111\\u1ea7u v\\u1ec1 kh\\u1ea3 n\\u0103ng T\\u1eacP TRUNG - GHI NH\\u1eda - PH\\u1ea2N X\\u1ea0 - T\\u1ef0 TIN\"]', 'vietnamese', 1, 3, '[26,27]', '[\"Mong mu\\u1ed1n tr\\u1ea3i nghi\\u1ec7m c\\u00f9ng ph\\u01b0\\u01a1ng FingerMath\",\"D\\u00e0nh cho tr\\u1ebb \\u0111ang trong \"]', 300000, 1, 150000, 'beginner', 6, NULL, 'https://youtu.be/z6nfBvrNyOA', 1585933200, 1587229200, NULL, 1, 0, 'active', 'youtube', '', '', NULL, 1, '', 'https://superbrain.edu.vn/san-pham/khoa-hoc-online-1-tuan');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `currency`
--

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `symbol` varchar(255) DEFAULT NULL,
  `paypal_supported` int(11) DEFAULT NULL,
  `stripe_supported` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `currency`
--

INSERT INTO `currency` (`id`, `name`, `code`, `symbol`, `paypal_supported`, `stripe_supported`) VALUES
(1, 'Leke', 'ALL', 'Lek', 0, 1),
(2, 'Dollars', 'USD', '$', 1, 1),
(3, 'Afghanis', 'AFN', '؋', 0, 1),
(4, 'Pesos', 'ARS', '$', 0, 1),
(5, 'Guilders', 'AWG', 'ƒ', 0, 1),
(6, 'Dollars', 'AUD', '$', 1, 1),
(7, 'New Manats', 'AZN', 'ман', 0, 1),
(8, 'Dollars', 'BSD', '$', 0, 1),
(9, 'Dollars', 'BBD', '$', 0, 1),
(10, 'Rubles', 'BYR', 'p.', 0, 0),
(11, 'Euro', 'EUR', '€', 1, 1),
(12, 'Dollars', 'BZD', 'BZ$', 0, 1),
(13, 'Dollars', 'BMD', '$', 0, 1),
(14, 'Bolivianos', 'BOB', '$b', 0, 1),
(15, 'Convertible Marka', 'BAM', 'KM', 0, 1),
(16, 'Pula', 'BWP', 'P', 0, 1),
(17, 'Leva', 'BGN', 'лв', 0, 1),
(18, 'Reais', 'BRL', 'R$', 1, 1),
(19, 'Pounds', 'GBP', '£', 1, 1),
(20, 'Dollars', 'BND', '$', 0, 1),
(21, 'Riels', 'KHR', '៛', 0, 1),
(22, 'Dollars', 'CAD', '$', 1, 1),
(23, 'Dollars', 'KYD', '$', 0, 1),
(24, 'Pesos', 'CLP', '$', 0, 1),
(25, 'Yuan Renminbi', 'CNY', '¥', 0, 1),
(26, 'Pesos', 'COP', '$', 0, 1),
(27, 'Colón', 'CRC', '₡', 0, 1),
(28, 'Kuna', 'HRK', 'kn', 0, 1),
(29, 'Pesos', 'CUP', '₱', 0, 0),
(30, 'Koruny', 'CZK', 'Kč', 1, 1),
(31, 'Kroner', 'DKK', 'kr', 1, 1),
(32, 'Pesos', 'DOP ', 'RD$', 0, 1),
(33, 'Dollars', 'XCD', '$', 0, 1),
(34, 'Pounds', 'EGP', '£', 0, 1),
(35, 'Colones', 'SVC', '$', 0, 0),
(36, 'Pounds', 'FKP', '£', 0, 1),
(37, 'Dollars', 'FJD', '$', 0, 1),
(38, 'Cedis', 'GHC', '¢', 0, 0),
(39, 'Pounds', 'GIP', '£', 0, 1),
(40, 'Quetzales', 'GTQ', 'Q', 0, 1),
(41, 'Pounds', 'GGP', '£', 0, 0),
(42, 'Dollars', 'GYD', '$', 0, 1),
(43, 'Lempiras', 'HNL', 'L', 0, 1),
(44, 'Dollars', 'HKD', '$', 1, 1),
(45, 'Forint', 'HUF', 'Ft', 1, 1),
(46, 'Kronur', 'ISK', 'kr', 0, 1),
(47, 'Rupees', 'INR', 'Rp', 1, 1),
(48, 'Rupiahs', 'IDR', 'Rp', 0, 1),
(49, 'Rials', 'IRR', '﷼', 0, 0),
(50, 'Pounds', 'IMP', '£', 0, 0),
(51, 'New Shekels', 'ILS', '₪', 1, 1),
(52, 'Dollars', 'JMD', 'J$', 0, 1),
(53, 'Yen', 'JPY', '¥', 1, 1),
(54, 'Pounds', 'JEP', '£', 0, 0),
(55, 'Tenge', 'KZT', 'лв', 0, 1),
(56, 'Won', 'KPW', '₩', 0, 0),
(57, 'Won', 'KRW', '₩', 0, 1),
(58, 'Soms', 'KGS', 'лв', 0, 1),
(59, 'Kips', 'LAK', '₭', 0, 1),
(60, 'Lati', 'LVL', 'Ls', 0, 0),
(61, 'Pounds', 'LBP', '£', 0, 1),
(62, 'Dollars', 'LRD', '$', 0, 1),
(63, 'Switzerland Francs', 'CHF', 'CHF', 1, 1),
(64, 'Litai', 'LTL', 'Lt', 0, 0),
(65, 'Denars', 'MKD', 'ден', 0, 1),
(66, 'Ringgits', 'MYR', 'RM', 1, 1),
(67, 'Rupees', 'MUR', '₨', 0, 1),
(68, 'Pesos', 'MXN', '$', 1, 1),
(69, 'Tugriks', 'MNT', '₮', 0, 1),
(70, 'Meticais', 'MZN', 'MT', 0, 1),
(71, 'Dollars', 'NAD', '$', 0, 1),
(72, 'Rupees', 'NPR', '₨', 0, 1),
(73, 'Guilders', 'ANG', 'ƒ', 0, 1),
(74, 'Dollars', 'NZD', '$', 1, 1),
(75, 'Cordobas', 'NIO', 'C$', 0, 1),
(76, 'Nairas', 'NGN', '₦', 0, 1),
(77, 'Krone', 'NOK', 'kr', 1, 1),
(78, 'Rials', 'OMR', '﷼', 0, 0),
(79, 'Rupees', 'PKR', '₨', 0, 1),
(80, 'Balboa', 'PAB', 'B/.', 0, 1),
(81, 'Guarani', 'PYG', 'Gs', 0, 1),
(82, 'Nuevos Soles', 'PEN', 'S/.', 0, 1),
(83, 'Pesos', 'PHP', 'Php', 1, 1),
(84, 'Zlotych', 'PLN', 'zł', 1, 1),
(85, 'Rials', 'QAR', '﷼', 0, 1),
(86, 'New Lei', 'RON', 'lei', 0, 1),
(87, 'Rubles', 'RUB', 'руб', 1, 1),
(88, 'Pounds', 'SHP', '£', 0, 1),
(89, 'Riyals', 'SAR', '﷼', 0, 1),
(90, 'Dinars', 'RSD', 'Дин.', 0, 1),
(91, 'Rupees', 'SCR', '₨', 0, 1),
(92, 'Dollars', 'SGD', '$', 1, 1),
(93, 'Dollars', 'SBD', '$', 0, 1),
(94, 'Shillings', 'SOS', 'S', 0, 1),
(95, 'Rand', 'ZAR', 'R', 0, 1),
(96, 'Rupees', 'LKR', '₨', 0, 1),
(97, 'Kronor', 'SEK', 'kr', 1, 1),
(98, 'Dollars', 'SRD', '$', 0, 1),
(99, 'Pounds', 'SYP', '£', 0, 0),
(100, 'New Dollars', 'TWD', 'NT$', 1, 1),
(101, 'Baht', 'THB', '฿', 1, 1),
(102, 'Dollars', 'TTD', 'TT$', 0, 1),
(103, 'Lira', 'TRY', 'TL', 0, 1),
(104, 'Liras', 'TRL', '£', 0, 0),
(105, 'Dollars', 'TVD', '$', 0, 0),
(106, 'Hryvnia', 'UAH', '₴', 0, 1),
(107, 'Pesos', 'UYU', '$U', 0, 1),
(108, 'Sums', 'UZS', 'лв', 0, 1),
(109, 'Bolivares Fuertes', 'VEF', 'Bs', 0, 0),
(110, 'Dong', 'VND', '₫', 0, 1),
(111, 'Rials', 'YER', '﷼', 0, 1),
(112, 'Zimbabwe Dollars', 'ZWD', 'Z$', 0, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `enrol`
--

CREATE TABLE `enrol` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `date_added` int(11) DEFAULT NULL,
  `last_modified` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `enrol`
--

INSERT INTO `enrol` (`id`, `user_id`, `course_id`, `date_added`, `last_modified`) VALUES
(2, 2, 2, 1584723600, NULL),
(3, 2, 1, 1584723600, NULL),
(9, 10, 3, 1586970000, NULL),
(10, 12, 3, 1587056400, NULL),
(11, 11, 3, 1587056400, NULL),
(12, 3, 3, 1587056400, NULL),
(13, 13, 3, 1587056400, NULL),
(18, 14, 3, 1587142800, NULL),
(19, 15, 3, 1587142800, NULL),
(20, 16, 3, 1587142800, NULL),
(21, 17, 3, 1587142800, NULL),
(22, 18, 3, 1587142800, NULL),
(23, 19, 3, 1587142800, NULL),
(24, 20, 3, 1587142800, NULL),
(25, 21, 3, 1587142800, NULL),
(26, 22, 3, 1587142800, NULL),
(27, 23, 3, 1587142800, NULL),
(28, 24, 3, 1587142800, NULL),
(29, 25, 3, 1587142800, NULL),
(30, 26, 3, 1587142800, NULL),
(31, 27, 3, 1587142800, NULL),
(32, 28, 3, 1587142800, NULL),
(33, 30, 3, 1587229200, NULL),
(34, 31, 3, 1587229200, NULL),
(35, 32, 3, 1587229200, NULL),
(36, 33, 3, 1587229200, NULL),
(37, 34, 3, 1587229200, NULL),
(38, 35, 3, 1587229200, NULL),
(39, 36, 3, 1587229200, NULL),
(40, 37, 3, 1587229200, NULL),
(41, 38, 3, 1587229200, NULL),
(42, 39, 3, 1587229200, NULL),
(43, 40, 3, 1587229200, NULL),
(44, 41, 3, 1587229200, NULL),
(45, 42, 3, 1587229200, NULL),
(46, 43, 3, 1587229200, NULL),
(47, 44, 3, 1587229200, NULL),
(48, 45, 3, 1587229200, NULL),
(49, 46, 3, 1587229200, NULL),
(50, 47, 3, 1587229200, NULL),
(51, 49, 3, 1587229200, NULL),
(52, 50, 3, 1587229200, NULL),
(53, 51, 3, 1587229200, NULL),
(54, 52, 3, 1587229200, NULL),
(55, 53, 3, 1587229200, NULL),
(56, 54, 3, 1587229200, NULL),
(57, 55, 3, 1587229200, NULL),
(58, 56, 3, 1587315600, NULL),
(59, 57, 3, 1587315600, NULL),
(60, 58, 3, 1587402000, NULL),
(61, 59, 3, 1587402000, NULL),
(63, 61, 3, 1587488400, NULL),
(64, 62, 3, 1587488400, NULL),
(65, 3, 1, 1587574800, NULL),
(66, 3, 2, 1587574800, NULL),
(67, 63, 3, 1587661200, NULL),
(68, 60, 1, 1587747600, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `frontend_settings`
--

CREATE TABLE `frontend_settings` (
  `id` int(11) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `frontend_settings`
--

INSERT INTO `frontend_settings` (`id`, `key`, `value`) VALUES
(1, 'banner_title', 'Chỉ 48h tạo nên sự khác biệt!'),
(2, 'banner_sub_title', 'Giúp trẻ Tập Trung, rèn luyện tính tự giác mỗi ngày thông qua Chương trình Toán Trí Tuệ Online'),
(4, 'about_us', '<p></p><p><span xss=removed>Câu chuyện Superbrain – Lá thư từ một người Mẹ</span></p><p><span xss=removed>Trải qua gần 10 năm phát triển, Superbrain tự hào với hơn 120 cơ sở đào tạo trên toàn đất nước, góp phần vào việc phát triển tư duy, trí tuệ cho hàng trăm ngàn trẻ em Việt Nam.</span></p><p><span xss=removed>Nhiều người đặt câu hỏi vì sao Hệ thống đào tạo Superbrain lại phát triển nhiều Cơ sở trên khắp cả nước như hiện nay. Lịch sử hình thành Superbrain Việt Nam là một câu chuyện của một người mẹ.</span></p><p><span xss=removed>Mời Quý phụ huynh chia sẻ “Câu chuyện Superbrain – Tâm thư của Người sáng lập Superbrain Việt Nam“!</span></p><p><span xss=removed>Câu chuyện Superbrain</span></p><p><span xss=removed>“Bé thường xuyên thiếu tập trung trong các hoạt động học tập, bé hay quên bài, bé thiếu tự tin, nhút nhát và khó hòa nhập với bạn bè.</span></p><p><span xss=removed>Đó là tất cả cảm giác lo lắng bất an mà tôi – một bà mẹ của ba đứa trẻ trải qua trong suốt một thời gian cùng với con trai thứ hai của mình. Thời điểm ấy là lúc tôi cùng gia đình trở về Việt Nam với rất nhiều sự bỡ ngỡ và mới mẻ, vừa phải sắp xếp cuộc sống, vừa phải lo lắng trường lớp cho các con, tôi luôn cảm thấy lo lắng cho tương lai của con trai. Nhưng trái tim của một bà mẹ mách bảo tôi rằng “Con trai tôi rất thông minh”, những điều bất ổn đang xảy ra với con trai tôi hẳn phải có lý do, chỉ cần biết được lý do, rồi mọi chuyện sẽ sớm được khắc phục.</span></p><p><span xss=removed>Bằng tất cả tình yêu của mình, tôi nghiên cứu rất nhiều tài liệu về trí tuệ và tư duy trẻ em, và những kiến thức ấy dẫn dắt tôi đến với phương pháp toán trí tuệ – nền tảng phát triển tư duy của trẻ bằng việc cân bằng sự phát triển của não bộ, sẽ là tiền đề cho mọi hoạt động mà con tham gia sau này. Rồi dường như có một phép màu nào đó, con trai tôi dần tiếp thu tốt hơn trong học tập, bé trở nên tự tin hơn, còn tôi thì cảm thấy an tâm hơn.</span></p><p><span xss=removed>Và nếu như bố mẹ đã và đang trăn trở câu chuyện giống như tôi, thì tôi tin Superbrain là nơi có thể mang lại cho bố mẹ những trải nghiệm tuyệt vời về sự tiến bộ của con từng ngày, điều mà tôi đã trải qua khi cùng con trai rèn luyện phương pháp toán trí tuệ.”</span></p><p><img src=\"https://superbrain.edu.vn/wp-content/uploads/2019/12/ANGIA-Global-Styles.png\"><span xss=removed><br></span></p><p><span xss=removed>Mrs. Siraya Thapanangkun,</span></p><p><span xss=removed>Người sáng lập Superbrain Việt Nam.</span><span xss=removed><br></span></p>'),
(10, 'terms_and_condition', '<div class=\"elementor-element elementor-element-1a0a914e elementor-widget elementor-widget-theme-post-title elementor-page-title elementor-widget-heading\" data-id=\"1a0a914e\" data-element_type=\"widget\" data-widget_type=\"theme-post-title.default\" xss=removed><div class=\"elementor-widget-container\" xss=removed><h1 class=\"elementor-heading-title elementor-size-default\" xss=removed>Chính sách mua hàng hoàn trả</h1></div></div><div class=\"elementor-element elementor-element-3b68fc1b elementor-widget elementor-widget-theme-post-content\" data-id=\"3b68fc1b\" data-element_type=\"widget\" data-widget_type=\"theme-post-content.default\" xss=removed><div class=\"elementor-widget-container\" xss=removed><p xss=removed>Hệ thống Toán Trí Tuệ Superbrain cam kết trẻ có sự tiến bộ thông qua các Khóa học. Vì thế, nếu Quý phụ huynh không nhận thấy trẻ có sự tiến bộ, Superbrain sẽ hoàn lại 100% học phí:</p><h4 xss=removed>1. Điều kiện hoàn học phí:</h4><p xss=removed>– Trong vòng 7  ngày kể từ ngày thanh toán.</p><p xss=removed>– Mua khóa học Superbrain Online và thanh toán trực tiếp với Superbrain.</p><h4 xss=removed>2. Cách thức gửi thông tin yêu cầu hoàn phí:</h4><p xss=removed>Để đảm bảo tính bảo mật và xác thực thông tin, trong trường hợp Phụ huynh muốn hoàn học phí, vui lòng thực hiện theo cách sau:</p><p xss=removed>– Quý phụ huynh sử dụng thông tin Email đã được dùng để đăng ký thanh toán khóa học, sau đó gửi yêu cầu vào<br>Email: online@superbrain.edu.vn và cung cấp các thông tin chi tiết như sau:</p><p xss=removed>+ Mã đơn hàng:</p><p xss=removed>+ Số tiền thanh toán:</p><p xss=removed>+ Phương thức thanh toán:</p><p xss=removed>– Nếu Quý phụ huynh đã thanh toán Khóa học bằng phương thức chuyển khoản vui lòng cung cấp đầy đủ thông tin tài khoản khi yêu cầu hoàn học phí gồm:</p><p xss=removed>+ Số tài khoản</p><p xss=removed>+ Chủ tài khoản</p><p xss=removed>+ Ngân hàng kèm tên chi nhánh.</p><h4 xss=removed>3. Thời gian hoàn học phí:</h4><p xss=removed>Quý phụ huynh sẽ nhận lại học phí trong vòng 3 – 7 ngày làm việc kể từ khi yêu cầu hoàn học phí được chấp nhận.</p><h4 xss=removed>4. Cách tính phần học phí hoàn lại:</h4><p xss=removed>Số tiền hoàn phí = Số tiền đã thanh toán – Phí đã thanh toán.</p></div></div><h2></h2>'),
(11, 'privacy_policy', '<h2><span xss=\"removed\">Chính sách bảo mật</span></h2><div><span xss=\"removed\">I. Chính sách bảo mật thông tin cá nhân:</span></div><div><span xss=\"removed\">Mục đích thu thập thông tin cá nhân:</span></div><div><span xss=\"removed\">Superbrain thu thập thông tin cá nhân chỉ cần thiết nhằm phục vụ cho các mục đích:</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">– Đơn hàng: để xử lý liên quan đến các vấn đề đặt hàng.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">– Duy trì tài khoản: để tạo và duy trì tài khoản của Quý phụ huynh với Superbrain</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">– Quản lý việc Quý phụ huynh sử dụng dịch vụ của Superbrain</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">– Để đáp ứng, xử lý, giải quyết hoặc hoàn tất một giao dịch</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">– Nhận dạng hoặc xác minh.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">– Báo cáo tiến bộ: Superbrain sẽ thường xuyên tương tác với Phụ huynh, báo cáo tình hình học tập và tiến độ của trẻ ở mỗi khóa học.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">– Gửi bài tập về nhà cho con mỗi ngày.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">Phạm vi thu thập:</span></div><div><span xss=\"removed\">Superbrain thu thập thông tin chỉ khi:</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">– Phụ huynh trực tiếp cung cấp: Họ tên, email, số điện thoại</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">Thời gian lưu trữ thông tin:</span></div><div><span xss=\"removed\">Thông tin cá nhân của khách hàng sẽ được lưu trữ cho đến khi khách hàng có yêu cầu hủy bỏ hoặc khách hàng tự đăng nhập và thực hiện hủy bỏ. Trong mọi trường hợp thông tin cá nhân của khách hàng sẽ được bảo mật trên máy chủ của Superbrain.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">Những người hoặc tổ chức có thể tiếp cận với thông tin đó:</span></div><div><span xss=\"removed\">Superbrain sẽ không cung cấp thông tin cá nhân của Phụ huynh hay của trẻ cho bất kỳ bên thứ ba nào, trừ một số hoạt động cần thiết dưới đây:</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">– Các đối tác là bên cung cấp dịch vụ cho chúng tôi liên quan đến thực hiện đơn hàng và chỉ giới hạn trong phạm vi thông tin cần thiết cũng như áp dụng các quy định đảm bảo an ninh, bảo mật các thông tin cá nhân.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">– Superbrain có thể sử dụng dịch vụ từ một nhà cung cấp dịch vụ là bên thứ ba để thực hiện một số hoạt động liên quan đến việc thanh toán và khi đó bên thứ ba này có thể truy cập hoặc xử lý các thông tin cá nhân trong quá trình cung cấp các dịch vụ đó. Superbrain yêu cầu các bên thứ ba này tuân thủ mọi luật lệ về bảo vệ thông tin cá nhân liên quan và các yêu cầu về an ninh liên quan đến thông tin cá nhân.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">– Yêu cầu pháp lý: Superbrain có thể tiết lộ các thông tin cá nhân nếu điều đó do luật pháp yêu cầu và việc tiết lộ như vậy là cần thiết một cách hợp lý để tuân thủ các quy trình pháp lý.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">– Chuyển giao kinh doanh (nếu có): trong trường hợp sáp nhập, hợp nhất toàn bộ hoặc một phần với công ty khác, người mua sẽ có quyền truy cập thông tin được Superbrain lưu trữ, duy trì trong đó bao gồm cả thông tin cá nhân.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">– Những trường hợp điền thiếu thông tin hoặc thông tin sai sự thật sẽ không được giải quyết khi có xảy ra các sự cố xảy ra với tài khoản.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">– Việc bảo mật thông tin: Quý phụ huynh có trách nhiệm tự mình bảo quản mật khẩu, nếu mật khẩu bị lộ ra ngoài dưới bất kỳ hình thức nào, Superbrain sẽ không chịu trách nhiệm về mọi tổn thất phát sinh.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">An toàn dữ liệu:</span></div><div><span xss=\"removed\">Superbrain luôn nỗ lực để giữ an toàn thông tin cá nhân của khách hàng, bằng nhiều biện pháp an toàn, bao gồm:</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">– Bảo đảm an toàn trong môi trường vận hành: Superbrain lưu trữ không tin cá nhân khách hàng trong môi trường vận hành an toàn và chỉ có nhân viên, đại diện và nhà cung cấp dịch vụ có thể truy cập trên cơ sở cần phải biết. Superbrain tuân theo các tiêu chuẩn ngành, pháp luật trong việc bảo mật thông tin cá nhân khách hàng.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">– Trong trường hợp máy chủ lưu trữ thông tin bị hacker tấn công dẫn đến mất mát dữ liệu cá nhân khách hàng, Superbrain sẽ có trách nhiệm thông báo vụ việc cho cơ quan chức năng để điều tra xử lý kịp thời và thông báo cho khách hàng được biết.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">– Các thông tin thanh toán: được bảo mật theo tiêu chuẩn ngành.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">Quyền của Quý phụ huynh đối với thông tin cá nhân:</span></div><div><span xss=\"removed\">– Có quyền cung cấp thông tin cá nhân cho chúng tôi và có thể thay đổi quyết định đó vào bất cứ lúc nào.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">– Có quyền tự kiểm tra, cập nhật, điều chỉnh thông tin cá nhân của mình bằng cách đăng nhập vào tài khoản và chỉnh sửa thông tin cá nhân hoặc yêu cầu Superbrain thực hiện việc này.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">Đơn vị thu thập và quản lý thông tin:</span></div><div><span xss=\"removed\">Tên công ty:  CÔNG TY CỔ PHẦN SUPERBRAIN GROUP</span></div><div><span xss=\"removed\">Thành lập và hoạt động theo Giấy chứng nhận đăng ký số 0315015641 do Sở kế hoạch và Đầu tư thành phố Hồ Chí Minh cấp đăng ký lần đầu ngày 03/05/2018</span></div><div><span xss=\"removed\">Trụ sở chính: Số 37, Đường 19, Khu phố 2, Phường Bình An, Quận 2, TP Hồ Chí Minh.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">Nếu Quý phụ huynh có bất kỳ thắc mắc nào về Chính sách này thì vui lòng liên hệ với chúng tôi qua:</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">– Hotline: 090 990 7311</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">– Email: superbrainvietnam@superbrain.edu.vn</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">II. Chính sách bảo mật thanh toán</span></div><div><span xss=\"removed\">Thông tin Cam kết bảo mật</span></div><div><span xss=\"removed\">Hệ thống thanh toán thẻ được cung cấp bởi các đối tác cổng thanh toán (“Đối Tác Cổng Thanh Toán”) đã được cấp phép hoạt động hợp pháp tại Việt Nam. Theo đó, các tiêu chuẩn bảo mật thanh toán thẻ tại Superbrain đảm bảo tuân thủ theo các tiêu chuẩn bảo mật ngành.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">Quy định bảo mật:</span></div><div><span xss=\"removed\">Chính sách giao dịch thanh toán bằng thẻ quốc tế và thẻ nội địa (internet banking) đảm bảo tuân thủ các tiêu chuẩn bảo mật của các Đối Tác Cổng Thanh Toán gồm:</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">– Thông tin tài chính của Khách hàng sẽ được bảo vệ trong suốt quá trình giao dịch bằng giao thức SSL (Secure Sockets Layer).</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">– Chứng nhận tiêu chuẩn bảo mật dữ liệu thông tin thanh toán (PCI DSS) do Trustwave cung cấp.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">–  Mật khẩu sử dụng một lần (OTP) được gửi qua SMS để đảm bảo việc truy cập tài khoản được xác thực.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">– Tiêu chuẩn mã hóa MD5 128 bit.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">– Các nguyên tắc và quy định bảo mật thông tin trong ngành tài chính ngân hàng theo quy định của Ngân hàng nhà nước Việt Nam.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">Chính sách bảo mật giao dịch trong thanh toán của Superbrain áp dụng với Khách hàng:</span></div><div><span xss=\"removed\">– Cung cấp tiện ích lưu giữ thông tin thẻ để sử dụng cho các lần thanh toán sau trên Superbrain.edu.vn với nguyên tắc chỉ lưu chuỗi đã được mã hóa bởi Đối Tác Cổng Thanh Toán cung cấp cho Superbrain, vì vậy Quý phụ huynh lựa chọn sử dụng tiện ích lưu giữ thông tin thẻ thì việc bảo mật thông tin thẻ thanh toán Khách hàng được thực hiện bởi Đối Tác Cổng Thanh Toán đã được cấp phép.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">– Đối với thẻ quốc tế: thông tin thẻ thanh toán của Khách hàng mà có khả năng sử dụng để xác lập giao dịch không được lưu trên hệ thống của superbrain.edu.vn. Đối Tác Cổng Thanh Toán sẽ lưu giữ và bảo mật theo tiêu chuẩn quốc tế PCI DSS.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">– Đối với thẻ nội địa (internet banking), Superbrain chỉ lưu trữ mã đơn hàng, mã giao dịch.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">– Superbrain cam kết đảm bảo thực hiện nghiêm túc các biện pháp bảo mật cần thiết cho mọi hoạt động thanh toán thực hiện trên Superbrain.edu.vn</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">Hệ thống Cơ sở Đào tạo Superbrain tuyệt vời đã xây dựng ứng dụng tuyệt vời trực tuyến như một ứng dụng miễn phí. DỊCH VỤ này được cung cấp bởi Superbrain và được dự định để sử dụng.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">Trang này được sử dụng để thông báo cho khách truy cập trang web về chính sách của chúng tôi với việc thu thập, sử dụng và tiết lộ Thông tin cá nhân nếu có ai quyết định sử dụng Dịch vụ của chúng tôi.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">Nếu bạn chọn sử dụng Dịch vụ của chúng tôi, thì bạn đồng ý với việc thu thập và sử dụng thông tin liên quan đến chính sách này. Thông tin cá nhân mà chúng tôi thu thập được sử dụng để cung cấp và cải thiện Dịch vụ. Chúng tôi sẽ không sử dụng hoặc chia sẻ thông tin của bạn với bất kỳ ai trừ khi được mô tả trong Chính sách bảo mật này.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">Các điều khoản được sử dụng trong Chính sách quyền riêng tư này có ý nghĩa tương tự như trong Điều khoản và Điều kiện của chúng tôi, có thể truy cập được tại Superbrain Online, trừ khi được quy định khác trong Chính sách bảo mật này.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">III. Thu thập và sử dụng thông tin</span></div><div><span xss=\"removed\">Để có trải nghiệm tốt hơn khi sử dụng Dịch vụ của chúng tôi, Chúng tôi có thể yêu cầu bạn cung cấp cho chúng tôi thông tin nhận dạng cá nhân nhất định, bao gồm nhưng không giới hạn ở [thêm bất cứ điều gì khác mà bạn thu thập ở đây, ví dụ: tên người dùng | địa chỉ | địa điểm | những bức ảnh]. Thông tin mà chúng tôi yêu cầu là [được lưu giữ trên thiết bị của bạn và không được chúng tôi thu thập dưới bất kỳ hình thức nào] | [sẽ được chúng tôi giữ lại và sử dụng như được mô tả trong chính sách bảo mật này.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">Ứng dụng sử dụng dịch vụ của bên thứ ba có thể thu thập thông tin được sử dụng để nhận dạng bạn.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">IV. Đăng nhập dữ liệu</span></div><div><span xss=\"removed\">Chúng tôi muốn thông báo cho bạn rằng bất cứ khi nào bạn sử dụng Dịch vụ của chúng tôi, trong trường hợp có lỗi trong ứng dụng Chúng tôi sẽ thu thập dữ liệu và thông tin (thông qua các sản phẩm của bên thứ ba) trên điện thoại của bạn có tên là Dữ liệu Đăng nhập. Dữ liệu nhật ký này có thể bao gồm thông tin như địa chỉ Giao thức Internet (Giao thức IP IP), tên thiết bị, phiên bản hệ điều hành, cấu hình của ứng dụng khi sử dụng Dịch vụ của chúng tôi, thời gian và ngày bạn sử dụng Dịch vụ và các thống kê khác.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">V. Cookie</span></div><div><span xss=\"removed\">Cookie là các tệp có lượng dữ liệu nhỏ thường được sử dụng một định danh duy nhất ẩn danh. Chúng được gửi đến trình duyệt của bạn từ trang web mà bạn truy cập và được lưu trên bộ nhớ trong của thiết bị.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">Dịch vụ này không sử dụng các cookie này một cách rõ ràng. Tuy nhiên, ứng dụng có thể sử dụng mã và thư viện của bên thứ ba sử dụng cookie của Hồi giáo để thu thập thông tin và cải thiện dịch vụ của họ. Bạn có tùy chọn chấp nhận hoặc từ chối các cookie này và biết khi nào cookie được gửi đến thiết bị của bạn. Nếu bạn chọn từ chối cookie của chúng tôi, bạn không thể sử dụng một số phần của Dịch vụ này.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">VI. Các nhà cung cấp dịch vụ</span></div><div><span xss=\"removed\">Chúng tôi có thể sử dụng các công ty và cá nhân bên thứ ba vì những lý do sau:</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">– Để tạo điều kiện cho Dịch vụ của chúng tôi;</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">– Để cung cấp Dịch vụ thay mặt chúng tôi;</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">– Để thực hiện các dịch vụ liên quan đến Dịch vụ; hoặc là</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">– Để hỗ trợ chúng tôi phân tích cách thức Dịch vụ của chúng tôi được sử dụng.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">Chúng tôi muốn thông báo cho người dùng Dịch vụ này rằng các bên thứ ba này có quyền truy cập vào Thông tin cá nhân của bạn. Lý do là để thực hiện các nhiệm vụ được giao cho họ thay mặt chúng tôi. Tuy nhiên, họ có nghĩa vụ không tiết lộ hoặc sử dụng thông tin cho bất kỳ mục đích nào khác.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">VII. Bảo vệ</span></div><div><span xss=\"removed\">Chúng tôi đánh giá sự tin tưởng của bạn trong việc cung cấp cho chúng tôi Thông tin cá nhân của bạn, do đó chúng tôi đang cố gắng sử dụng các phương tiện bảo vệ thương mại có thể chấp nhận được. Nhưng hãy nhớ rằng không có phương thức truyền qua internet, hoặc phương pháp lưu trữ điện tử nào an toàn và đáng tin cậy 100%, và chúng tôi không thể đảm bảo tính bảo mật tuyệt đối của nó.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">VIII. Liên kết đến các trang web khác</span></div><div><span xss=\"removed\">Dịch vụ này có thể chứa các liên kết đến các trang web khác. Nếu bạn nhấp vào liên kết của bên thứ ba, bạn sẽ được chuyển đến trang web đó. Lưu ý rằng các trang web bên ngoài không được vận hành bởi chúng tôi. Do đó, tôi thực sự khuyên bạn nên xem lại Chính sách quyền riêng tư của các trang web này. Tôi không kiểm soát và không chịu trách nhiệm về nội dung, chính sách bảo mật hoặc thực tiễn của bất kỳ trang web hoặc dịch vụ của bên thứ ba nào.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">IX. Quyền riêng tư của trẻ em</span></div><div><span xss=\"removed\">Dịch vụ này không giải quyết cho bất kỳ ai dưới 13 tuổi. Chúng tôi không cố ý thu thập thông tin cá nhân của trẻ em dưới 13 tuổi. Trong trường hợp chúng tôi phát hiện ra rằng một đứa trẻ dưới 13 tuổi đã cung cấp cho chúng tôi thông tin cá nhân, chúng tôi xóa ngay lập tức khỏi máy chủ của chúng tôi. Nếu bạn là cha mẹ hoặc người giám hộ và bạn biết rằng con bạn đã cung cấp cho chúng tôi thông tin cá nhân, vui lòng liên hệ với chúng tôi để chúng tôi có thể thực hiện các hành động cần thiết.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">X. Thay đổi chính sách bảo mật này</span></div><div><span xss=\"removed\">Chúng tôi có thể cập nhật Chính sách bảo mật của chúng tôi theo thời gian. Vì vậy, bạn nên xem lại trang này định kỳ cho bất kỳ thay đổi. Chúng tôi sẽ thông báo cho bạn về bất kỳ thay đổi nào bằng cách đăng Chính sách bảo mật mới trên trang này. Những thay đổi này có hiệu lực ngay lập tức, sau khi chúng được đăng trên trang này.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">XII. Liên hệ chúng tôi</span></div><div><span xss=\"removed\">Nếu bạn có bất kỳ câu hỏi hoặc đề xuất nào về Chính sách bảo mật của chúng tôi, đừng ngần ngại liên hệ với chúng tôi.</span></div><div><span xss=\"removed\"><br></span></div><div><span xss=\"removed\">Trang Chính sách bảo mật này đã được tạo ra tại superbrain.edu.vn.</span></div>'),
(13, 'theme', 'superbrain'),
(14, 'cookie_note', 'Cookie là các tệp có lượng dữ liệu nhỏ thường được sử dụng một định danh duy nhất ẩn danh. Chúng được gửi đến trình duyệt của bạn từ trang web mà bạn truy cập và được lưu trên bộ nhớ trong của thiết bị.'),
(15, 'cookie_status', 'inactive'),
(16, 'cookie_policy', '<div>Cookie là các tệp có lượng dữ liệu nhỏ thường được sử dụng một định danh duy nhất ẩn danh. Chúng được gửi đến trình duyệt của bạn từ trang web mà bạn truy cập và được lưu trên bộ nhớ trong của thiết bị.</div><div><br></div><div>Dịch vụ này không sử dụng các cookie này một cách rõ ràng. Tuy nhiên, ứng dụng có thể sử dụng mã và thư viện của bên thứ ba sử dụng cookie của Hồi giáo để thu thập thông tin và cải thiện dịch vụ của họ. Bạn có tùy chọn chấp nhận hoặc từ chối các cookie này và biết khi nào cookie được gửi đến thiết bị của bạn. Nếu bạn chọn từ chối cookie của chúng tôi, bạn không thể sử dụng một số phần của Dịch vụ này.</div>');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `language`
--

CREATE TABLE `language` (
  `phrase_id` int(11) NOT NULL,
  `phrase` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `english` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `Bengali` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `language`
--

INSERT INTO `language` (`phrase_id`, `phrase`, `english`, `Bengali`) VALUES
(1, 'manage_profile', NULL, NULL),
(3, 'dashboard', NULL, NULL),
(4, 'categories', NULL, NULL),
(5, 'courses', NULL, NULL),
(6, 'students', NULL, NULL),
(7, 'enroll_history', NULL, NULL),
(8, 'message', NULL, NULL),
(9, 'settings', NULL, NULL),
(10, 'system_settings', NULL, NULL),
(11, 'frontend_settings', NULL, NULL),
(12, 'payment_settings', NULL, NULL),
(13, 'manage_language', NULL, NULL),
(14, 'edit_profile', NULL, NULL),
(15, 'log_out', NULL, NULL),
(16, 'first_name', NULL, NULL),
(17, 'last_name', NULL, NULL),
(18, 'email', NULL, NULL),
(19, 'facebook_link', NULL, NULL),
(20, 'twitter_link', NULL, NULL),
(21, 'linkedin_link', NULL, NULL),
(22, 'a_short_title_about_yourself', NULL, NULL),
(23, 'biography', NULL, NULL),
(24, 'photo', NULL, NULL),
(25, 'select_image', NULL, NULL),
(26, 'change', NULL, NULL),
(27, 'remove', NULL, NULL),
(28, 'update_profile', NULL, NULL),
(29, 'change_password', NULL, NULL),
(30, 'current_password', NULL, NULL),
(31, 'new_password', NULL, NULL),
(32, 'confirm_new_password', NULL, NULL),
(33, 'delete', NULL, NULL),
(34, 'cancel', NULL, NULL),
(35, 'are_you_sure_to_update_this_information', NULL, NULL),
(36, 'yes', NULL, NULL),
(37, 'no', NULL, NULL),
(38, 'system settings', NULL, NULL),
(39, 'system_name', NULL, NULL),
(40, 'system_title', NULL, NULL),
(41, 'slogan', NULL, NULL),
(42, 'system_email', NULL, NULL),
(43, 'address', NULL, NULL),
(44, 'phone', NULL, NULL),
(45, 'youtube_api_key', NULL, NULL),
(46, 'get_youtube_api_key', NULL, NULL),
(47, 'vimeo_api_key', NULL, NULL),
(48, 'purchase_code', NULL, NULL),
(49, 'language', NULL, NULL),
(50, 'text-align', NULL, NULL),
(51, 'update_system_settings', NULL, NULL),
(52, 'update_product', NULL, NULL),
(53, 'file', NULL, NULL),
(54, 'install_update', NULL, NULL),
(55, 'system_logo', NULL, NULL),
(56, 'update_logo', NULL, NULL),
(57, 'get_vimeo_api_key', NULL, NULL),
(58, 'add_category', NULL, NULL),
(59, 'category_title', NULL, NULL),
(60, 'sub_categories', NULL, NULL),
(61, 'actions', NULL, NULL),
(62, 'action', NULL, NULL),
(63, 'manage_sub_categories', NULL, NULL),
(64, 'edit', NULL, NULL),
(65, 'add_course', NULL, NULL),
(66, 'select_category', NULL, NULL),
(67, 'title', NULL, NULL),
(68, 'category', NULL, NULL),
(69, '#_section', NULL, NULL),
(70, '#_lesson', NULL, NULL),
(71, '#_enrolled_user', NULL, NULL),
(72, 'view_course_details', NULL, NULL),
(73, 'manage_section', NULL, NULL),
(74, 'manage_lesson', NULL, NULL),
(75, 'student', NULL, NULL),
(76, 'add_student', NULL, NULL),
(77, 'name', NULL, NULL),
(78, 'date_added', NULL, NULL),
(79, 'enrolled_courses', NULL, NULL),
(80, 'view_profile', NULL, NULL),
(81, 'admin_dashboard', NULL, NULL),
(82, 'total_courses', NULL, NULL),
(83, 'number_of_courses', NULL, NULL),
(84, 'total_lessons', NULL, NULL),
(85, 'number_of_lessons', NULL, NULL),
(86, 'total_enrollment', NULL, NULL),
(87, 'number_of_enrollment', NULL, NULL),
(88, 'total_student', NULL, NULL),
(89, 'number_of_student', NULL, NULL),
(90, 'manage_sections', NULL, NULL),
(91, 'manage sections', NULL, NULL),
(92, 'course', NULL, NULL),
(93, 'add_section', NULL, NULL),
(94, 'lessons', NULL, NULL),
(95, 'serialize_sections', NULL, NULL),
(96, 'add_lesson', NULL, NULL),
(97, 'edit_section', NULL, NULL),
(98, 'delete_section', NULL, NULL),
(99, 'course_details', NULL, NULL),
(100, 'course details', NULL, NULL),
(101, 'details', NULL, NULL),
(102, 'instructor', NULL, NULL),
(103, 'sub_category', NULL, NULL),
(104, 'enrolled_user', NULL, NULL),
(105, 'last_modified', NULL, NULL),
(106, 'manage language', NULL, NULL),
(107, 'language_list', NULL, NULL),
(108, 'add_phrase', NULL, NULL),
(109, 'add_language', NULL, NULL),
(110, 'option', NULL, NULL),
(111, 'edit_phrase', NULL, NULL),
(112, 'delete_language', NULL, NULL),
(113, 'phrase', NULL, NULL),
(114, 'value_required', NULL, NULL),
(115, 'frontend settings', NULL, NULL),
(116, 'banner_title', NULL, NULL),
(117, 'banner_sub_title', NULL, NULL),
(118, 'about_us', NULL, NULL),
(119, 'blog', NULL, NULL),
(120, 'update_frontend_settings', NULL, NULL),
(121, 'update_banner', NULL, NULL),
(122, 'banner_image', NULL, NULL),
(123, 'update_banner_image', NULL, NULL),
(124, 'payment settings', NULL, NULL),
(125, 'paypal_settings', NULL, NULL),
(126, 'client_id', NULL, NULL),
(127, 'sandbox', NULL, NULL),
(128, 'production', NULL, NULL),
(129, 'active', NULL, NULL),
(130, 'mode', NULL, NULL),
(131, 'stripe_settings', NULL, NULL),
(132, 'testmode', NULL, NULL),
(133, 'on', NULL, NULL),
(134, 'off', NULL, NULL),
(135, 'test_secret_key', NULL, NULL),
(136, 'test_public_key', NULL, NULL),
(137, 'live_secret_key', NULL, NULL),
(138, 'live_public_key', NULL, NULL),
(139, 'save_changes', NULL, NULL),
(140, 'category_code', NULL, NULL),
(141, 'update_phrase', NULL, NULL),
(142, 'check', NULL, NULL),
(143, 'settings_updated', NULL, NULL),
(144, 'checking', NULL, NULL),
(145, 'phrase_added', NULL, NULL),
(146, 'language_added', NULL, NULL),
(147, 'language_deleted', NULL, NULL),
(148, 'add course', NULL, NULL),
(149, 'add_courses', NULL, NULL),
(150, 'add_course_form', NULL, NULL),
(151, 'basic_info', NULL, NULL),
(152, 'short_description', NULL, NULL),
(153, 'description', NULL, NULL),
(154, 'level', NULL, NULL),
(155, 'beginner', NULL, NULL),
(156, 'advanced', NULL, NULL),
(157, 'intermediate', NULL, NULL),
(158, 'language_made_in', NULL, NULL),
(159, 'is_top_course', NULL, NULL),
(160, 'outcomes', NULL, NULL),
(161, 'category_and_sub_category', NULL, NULL),
(162, 'select_a_category', NULL, NULL),
(163, 'select_a_category_first', NULL, NULL),
(164, 'requirements', NULL, NULL),
(165, 'price_and_discount', NULL, NULL),
(166, 'price', NULL, NULL),
(167, 'has_discount', NULL, NULL),
(168, 'discounted_price', NULL, NULL),
(169, 'course_thumbnail', NULL, NULL),
(170, 'note', NULL, NULL),
(171, 'thumbnail_size_should_be_600_x_600', NULL, NULL),
(172, 'course_overview_url', NULL, NULL),
(173, '0%', NULL, NULL),
(174, 'manage profile', NULL, NULL),
(175, 'edit_course', NULL, NULL),
(176, 'edit course', NULL, NULL),
(177, 'edit_courses', NULL, NULL),
(178, 'edit_course_form', NULL, NULL),
(179, 'update_course', NULL, NULL),
(180, 'course_updated', NULL, NULL),
(181, 'number_of_sections', NULL, NULL),
(182, 'number_of_enrolled_users', NULL, NULL),
(183, 'add section', NULL, NULL),
(184, 'section', NULL, NULL),
(185, 'add_section_form', NULL, NULL),
(186, 'update', NULL, NULL),
(187, 'serialize_section', NULL, NULL),
(188, 'serialize section', NULL, NULL),
(189, 'submit', NULL, NULL),
(190, 'sections_have_been_serialized', NULL, NULL),
(191, 'select_course', NULL, NULL),
(192, 'search', NULL, NULL),
(193, 'thumbnail', NULL, NULL),
(194, 'duration', NULL, NULL),
(195, 'provider', NULL, NULL),
(196, 'add lesson', NULL, NULL),
(197, 'add_lesson_form', NULL, NULL),
(198, 'video_type', NULL, NULL),
(199, 'select_a_course', NULL, NULL),
(200, 'select_a_course_first', NULL, NULL),
(201, 'video_url', NULL, NULL),
(202, 'invalid_url', NULL, NULL),
(203, 'your_video_source_has_to_be_either_youtube_or_vimeo', NULL, NULL),
(204, 'for', NULL, NULL),
(205, 'of', NULL, NULL),
(206, 'edit_lesson', NULL, NULL),
(207, 'edit lesson', NULL, NULL),
(208, 'edit_lesson_form', NULL, NULL),
(209, 'login', NULL, NULL),
(210, 'password', NULL, NULL),
(211, 'forgot_password', NULL, NULL),
(212, 'back_to_website', NULL, NULL),
(213, 'send_mail', NULL, NULL),
(214, 'back_to_login', NULL, NULL),
(215, 'student_add', NULL, NULL),
(216, 'student add', NULL, NULL),
(217, 'add_students', NULL, NULL),
(218, 'student_add_form', NULL, NULL),
(219, 'login_credentials', NULL, NULL),
(220, 'social_information', NULL, NULL),
(221, 'facebook', NULL, NULL),
(222, 'twitter', NULL, NULL),
(223, 'linkedin', NULL, NULL),
(224, 'user_image', NULL, NULL),
(225, 'add_user', NULL, NULL),
(226, 'user_update_successfully', NULL, NULL),
(227, 'user_added_successfully', NULL, NULL),
(228, 'student_edit', NULL, NULL),
(229, 'student edit', NULL, NULL),
(230, 'edit_students', NULL, NULL),
(231, 'student_edit_form', NULL, NULL),
(232, 'update_user', NULL, NULL),
(233, 'enroll history', NULL, NULL),
(234, 'filter', NULL, NULL),
(235, 'user_name', NULL, NULL),
(236, 'enrolled_course', NULL, NULL),
(237, 'enrollment_date', NULL, NULL),
(238, 'biography2', NULL, NULL),
(239, 'home', NULL, NULL),
(240, 'search_for_courses', NULL, NULL),
(241, 'total', NULL, NULL),
(242, 'go_to_cart', NULL, NULL),
(243, 'your_cart_is_empty', NULL, NULL),
(244, 'log_in', NULL, NULL),
(245, 'sign_up', NULL, NULL),
(246, 'what_do_you_want_to_learn', NULL, NULL),
(247, 'online_courses', NULL, NULL),
(248, 'explore_a_variety_of_fresh_topics', NULL, NULL),
(249, 'expert_instruction', NULL, NULL),
(250, 'find_the_right_course_for_you', NULL, NULL),
(251, 'lifetime_access', NULL, NULL),
(252, 'learn_on_your_schedule', NULL, NULL),
(253, 'top_courses', NULL, NULL),
(254, 'last_updater', NULL, NULL),
(255, 'hours', NULL, NULL),
(256, 'add_to_cart', NULL, NULL),
(257, 'top', NULL, NULL),
(258, 'latest_courses', NULL, NULL),
(259, 'added_to_cart', NULL, NULL),
(260, 'admin', NULL, NULL),
(261, 'log_in_to_your_udemy_account', NULL, NULL),
(262, 'by_signing_up_you_agree_to_our', NULL, NULL),
(263, 'terms_of_use', NULL, NULL),
(264, 'and', NULL, NULL),
(265, 'privacy_policy', NULL, NULL),
(266, 'do_not_have_an_account', NULL, NULL),
(267, 'sign_up_and_start_learning', NULL, NULL),
(268, 'check_here_for_exciting_deals_and_personalized_course_recommendations', NULL, NULL),
(269, 'already_have_an_account', NULL, NULL),
(270, 'checkout', NULL, NULL),
(271, 'paypal', NULL, NULL),
(272, 'stripe', NULL, NULL),
(273, 'step', NULL, NULL),
(274, 'how_would_you_rate_this_course_overall', NULL, NULL),
(275, 'write_a_public_review', NULL, NULL),
(276, 'describe_your_experience_what_you_got_out_of_the_course_and_other_helpful_highlights', NULL, NULL),
(277, 'what_did_the_instructor_do_well_and_what_could_use_some_improvement', NULL, NULL),
(278, 'next', NULL, NULL),
(279, 'previous', NULL, NULL),
(280, 'publish', NULL, NULL),
(281, 'search_results', NULL, NULL),
(282, 'ratings', NULL, NULL),
(283, 'search_results_for', NULL, NULL),
(284, 'category_page', NULL, NULL),
(285, 'all', NULL, NULL),
(286, 'category_list', NULL, NULL),
(287, 'by', NULL, NULL),
(288, 'go_to_wishlist', NULL, NULL),
(289, 'hi', NULL, NULL),
(290, 'my_courses', NULL, NULL),
(291, 'my_wishlist', NULL, NULL),
(292, 'my_messages', NULL, NULL),
(293, 'purchase_history', NULL, NULL),
(294, 'user_profile', NULL, NULL),
(295, 'already_purchased', NULL, NULL),
(296, 'all_courses', NULL, NULL),
(297, 'wishlists', NULL, NULL),
(298, 'search_my_courses', NULL, NULL),
(299, 'students_enrolled', NULL, NULL),
(300, 'created_by', NULL, NULL),
(301, 'last_updated', NULL, NULL),
(302, 'what_will_i_learn', NULL, NULL),
(303, 'view_more', NULL, NULL),
(304, 'other_related_courses', NULL, NULL),
(305, 'updated', NULL, NULL),
(306, 'curriculum_for_this_course', NULL, NULL),
(307, 'about_the_instructor', NULL, NULL),
(308, 'reviews', NULL, NULL),
(309, 'student_feedback', NULL, NULL),
(310, 'average_rating', NULL, NULL),
(311, 'preview_this_course', NULL, NULL),
(312, 'includes', NULL, NULL),
(313, 'on_demand_videos', NULL, NULL),
(314, 'full_lifetime_access', NULL, NULL),
(315, 'access_on_mobile_and_tv', NULL, NULL),
(316, 'course_preview', NULL, NULL),
(317, 'instructor_page', NULL, NULL),
(318, 'buy_now', NULL, NULL),
(319, 'shopping_cart', NULL, NULL),
(320, 'courses_in_cart', NULL, NULL),
(321, 'student_name', NULL, NULL),
(322, 'amount_to_pay', NULL, NULL),
(323, 'payment_successfully_done', NULL, NULL),
(324, 'filter_by', NULL, NULL),
(325, 'instructors', NULL, NULL),
(326, 'reset', NULL, NULL),
(327, 'your', NULL, NULL),
(328, 'rating', NULL, NULL),
(329, 'course_detail', NULL, NULL),
(330, 'start_lesson', NULL, NULL),
(331, 'show_full_biography', NULL, NULL),
(332, 'terms_and_condition', NULL, NULL),
(333, 'about', NULL, NULL),
(334, 'terms_&_condition', NULL, NULL),
(335, 'sub categories', NULL, NULL),
(336, 'add_sub_category', NULL, NULL),
(337, 'sub_category_title', NULL, NULL),
(338, 'add sub category', NULL, NULL),
(339, 'add_sub_category_form', NULL, NULL),
(340, 'sub_category_code', NULL, NULL),
(341, 'data_deleted', NULL, NULL),
(342, 'edit_category', NULL, NULL),
(343, 'edit category', NULL, NULL),
(344, 'edit_category_form', NULL, NULL),
(345, 'font', NULL, NULL),
(346, 'awesome class', NULL, NULL),
(347, 'update_category', NULL, NULL),
(348, 'data_updated_successfully', NULL, NULL),
(349, 'edit_sub_category', NULL, NULL),
(350, 'edit sub category', NULL, NULL),
(351, 'sub_category_edit', NULL, NULL),
(352, 'update_sub_category', NULL, NULL),
(353, 'course_added', NULL, NULL),
(354, 'user_deleted_successfully', NULL, NULL),
(355, 'private_messaging', NULL, NULL),
(356, 'private messaging', NULL, NULL),
(357, 'messages', NULL, NULL),
(358, 'select_message_to_read', NULL, NULL),
(359, 'new_message', NULL, NULL),
(360, 'email_duplication', NULL, NULL),
(361, 'your_registration_has_been_successfully_done', NULL, NULL),
(362, 'profile', NULL, NULL),
(363, 'account', NULL, NULL),
(364, 'add_information_about_yourself_to_share_on_your_profile', NULL, NULL),
(365, 'basics', NULL, NULL),
(366, 'add_your_twitter_link', NULL, NULL),
(367, 'add_your_facebook_link', NULL, NULL),
(368, 'add_your_linkedin_link', NULL, NULL),
(369, 'credentials', NULL, NULL),
(370, 'edit_your_account_settings', NULL, NULL),
(371, 'enter_current_password', NULL, NULL),
(372, 'enter_new_password', NULL, NULL),
(373, 're-type_your_password', NULL, NULL),
(374, 'save', NULL, NULL),
(375, 'update_user_photo', NULL, NULL),
(376, 'update_your_photo', NULL, NULL),
(377, 'upload_image', NULL, NULL),
(378, 'updated_successfully', NULL, NULL),
(379, 'invalid_login_credentials', NULL, NULL),
(380, 'blank_page', NULL, NULL),
(381, 'no_section_found', NULL, NULL),
(382, 'select_a_message_thread_to_read_it_here', NULL, NULL),
(383, 'send', NULL, NULL),
(384, 'type_your_message', NULL, NULL),
(385, 'date', NULL, NULL),
(386, 'total_price', NULL, NULL),
(387, 'payment_type', NULL, NULL),
(388, 'edit section', NULL, NULL),
(389, 'edit_section_form', NULL, NULL),
(390, 'reply_message', NULL, NULL),
(391, 'reply', NULL, NULL),
(392, 'log_in_to_your_account', NULL, NULL),
(393, 'no_result_found', NULL, NULL),
(394, 'enrollment', NULL, NULL),
(395, 'enroll_a_student', NULL, NULL),
(396, 'report', NULL, NULL),
(397, 'admin_revenue', NULL, NULL),
(398, 'instructor_revenue', NULL, NULL),
(399, 'instructor_settings', NULL, NULL),
(400, 'view_frontend', NULL, NULL),
(401, 'number_of_active_courses', NULL, NULL),
(402, 'number_of_pending_courses', NULL, NULL),
(403, 'all_instructor', NULL, NULL),
(404, 'active_courses', NULL, NULL),
(405, 'pending_courses', NULL, NULL),
(406, 'no_data_found', NULL, NULL),
(407, 'view_course_on_frontend', NULL, NULL),
(408, 'mark_as_pending', NULL, NULL),
(409, 'add category', NULL, NULL),
(410, 'add_categories', NULL, NULL),
(411, 'category_add_form', NULL, NULL),
(412, 'icon_picker', NULL, NULL),
(413, 'enroll a student', NULL, NULL),
(414, 'enrollment_form', NULL, NULL),
(415, 'admin revenue', NULL, NULL),
(416, 'total_amount', NULL, NULL),
(417, 'instructor revenue', NULL, NULL),
(418, 'status', NULL, NULL),
(419, 'instructor settings', NULL, NULL),
(420, 'allow_public_instructor', NULL, NULL),
(421, 'instructor_revenue_percentage', NULL, NULL),
(422, 'admin_revenue_percentage', NULL, NULL),
(423, 'update_instructor_settings', NULL, NULL),
(424, 'payment_info', NULL, NULL),
(425, 'required_for_instructors', NULL, NULL),
(426, 'paypal_client_id', NULL, NULL),
(427, 'stripe_public_key', NULL, NULL),
(428, 'stripe_secret_key', NULL, NULL),
(429, 'mark_as_active', NULL, NULL),
(430, 'mail_subject', NULL, NULL),
(431, 'mail_body', NULL, NULL),
(432, 'paid', NULL, NULL),
(433, 'pending', NULL, NULL),
(434, 'this_instructor_has_not_provided_valid_paypal_client_id', NULL, NULL),
(435, 'pay_with_paypal', NULL, NULL),
(436, 'this_instructor_has_not_provided_valid_public_key_or_secret_key', NULL, NULL),
(437, 'pay_with_stripe', NULL, NULL),
(438, 'create_course', NULL, NULL),
(439, 'payment_report', NULL, NULL),
(440, 'instructor_dashboard', NULL, NULL),
(441, 'draft', NULL, NULL),
(442, 'view_lessons', NULL, NULL),
(443, 'course_title', NULL, NULL),
(444, 'update_your_payment_settings', NULL, NULL),
(445, 'edit_course_detail', NULL, NULL),
(446, 'edit_basic_informations', NULL, NULL),
(447, 'publish_this_course', NULL, NULL),
(448, 'save_to_draft', NULL, NULL),
(449, 'update_section', NULL, NULL),
(450, 'analyzing_given_url', NULL, NULL),
(451, 'select_a_section', NULL, NULL),
(452, 'update_lesson', NULL, NULL),
(453, 'website_name', NULL, NULL),
(454, 'website_title', NULL, NULL),
(455, 'website_keywords', NULL, NULL),
(456, 'website_description', NULL, NULL),
(457, 'author', NULL, NULL),
(458, 'footer_text', NULL, NULL),
(459, 'footer_link', NULL, NULL),
(460, 'update_backend_logo', NULL, NULL),
(461, 'update_favicon', NULL, NULL),
(462, 'favicon', NULL, NULL),
(463, 'active courses', NULL, NULL),
(464, 'product_updated_successfully', NULL, NULL),
(465, 'course_overview_provider', NULL, NULL),
(466, 'youtube', NULL, NULL),
(467, 'vimeo', NULL, NULL),
(468, 'html5', NULL, NULL),
(469, 'meta_keywords', NULL, NULL),
(470, 'meta_description', NULL, NULL),
(471, 'lesson_type', NULL, NULL),
(472, 'video', NULL, NULL),
(473, 'select_type_of_lesson', NULL, NULL),
(474, 'text_file', NULL, NULL),
(475, 'pdf_file', NULL, NULL),
(476, 'document_file', NULL, NULL),
(477, 'image_file', NULL, NULL),
(478, 'lesson_provider', NULL, NULL),
(479, 'select_lesson_provider', NULL, NULL),
(480, 'analyzing_the_url', NULL, NULL),
(481, 'attachment', NULL, NULL),
(482, 'summary', NULL, NULL),
(483, 'download', NULL, NULL),
(484, 'system_settings_updated', NULL, NULL),
(485, 'course_updated_successfully', NULL, NULL),
(486, 'please_wait_untill_admin_approves_it', NULL, NULL),
(487, 'pending courses', NULL, NULL),
(488, 'course_status_updated', NULL, NULL),
(489, 'smtp_settings', NULL, NULL),
(490, 'free_course', NULL, NULL),
(491, 'free', NULL, NULL),
(492, 'get_enrolled', NULL, NULL),
(493, 'course_added_successfully', NULL, NULL),
(494, 'update_frontend_logo', NULL, NULL),
(495, 'system_currency_settings', NULL, NULL),
(496, 'select_system_currency', NULL, NULL),
(497, 'currency_position', NULL, NULL),
(498, 'left', NULL, NULL),
(499, 'right', NULL, NULL),
(500, 'left_with_a_space', NULL, NULL),
(501, 'right_with_a_space', NULL, NULL),
(502, 'paypal_currency', NULL, NULL),
(503, 'select_paypal_currency', NULL, NULL),
(504, 'stripe_currency', NULL, NULL),
(505, 'select_stripe_currency', NULL, NULL),
(506, 'heads_up', NULL, NULL),
(507, 'please_make_sure_that', NULL, NULL),
(508, 'system_currency', NULL, NULL),
(509, 'are_same', NULL, NULL),
(510, 'smtp settings', NULL, NULL),
(511, 'protocol', NULL, NULL),
(512, 'smtp_host', NULL, NULL),
(513, 'smtp_port', NULL, NULL),
(514, 'smtp_user', NULL, NULL),
(515, 'smtp_pass', NULL, NULL),
(516, 'update_smtp_settings', NULL, NULL),
(517, 'phrase_updated', NULL, NULL),
(518, 'registered_user', NULL, NULL),
(519, 'provide_your_valid_login_credentials', NULL, NULL),
(520, 'registration_form', NULL, NULL),
(521, 'provide_your_email_address_to_get_password', NULL, NULL),
(522, 'reset_password', NULL, NULL),
(523, 'want_to_go_back', NULL, NULL),
(524, 'message_sent!', NULL, NULL),
(525, 'selected_icon', NULL, NULL),
(526, 'pick_another_icon_picker', NULL, NULL),
(527, 'show_more', NULL, NULL),
(528, 'show_less', NULL, NULL),
(529, 'all_category', NULL, NULL),
(530, 'price_range', NULL, NULL),
(531, 'price_range_withing', NULL, NULL),
(532, 'all_categories', NULL, NULL),
(533, 'all_sub_category', NULL, NULL),
(534, 'number_of_results', NULL, NULL),
(535, 'showing_on_this_page', NULL, NULL),
(536, 'welcome', NULL, NULL),
(537, 'my_account', NULL, NULL),
(538, 'logout', NULL, NULL),
(539, 'visit_website', NULL, NULL),
(540, 'navigation', NULL, NULL),
(541, 'add_new_category', NULL, NULL),
(542, 'enrolment', NULL, NULL),
(543, 'enrol_history', NULL, NULL),
(544, 'enrol_a_student', NULL, NULL),
(545, 'language_settings', NULL, NULL),
(546, 'congratulations', NULL, NULL),
(547, 'oh_snap', NULL, NULL),
(548, 'close', NULL, NULL),
(549, 'parent', NULL, NULL),
(550, 'none', NULL, NULL),
(551, 'category_thumbnail', NULL, NULL),
(552, 'the_image_size_should_be', NULL, NULL),
(553, 'choose_thumbnail', NULL, NULL),
(554, 'data_added_successfully', NULL, NULL),
(555, '', NULL, NULL),
(556, 'update_category_form', NULL, NULL),
(557, 'student_list', NULL, NULL),
(558, 'choose_user_image', NULL, NULL),
(559, 'finish', NULL, NULL),
(560, 'thank_you', NULL, NULL),
(561, 'you_are_almost_there', NULL, NULL),
(562, 'you_are_just_one_click_away', NULL, NULL),
(563, 'country', NULL, NULL),
(564, 'website_settings', NULL, NULL),
(565, 'write_down_facebook_url', NULL, NULL),
(566, 'write_down_twitter_url', NULL, NULL),
(567, 'write_down_linkedin_url', NULL, NULL),
(568, 'google_link', NULL, NULL),
(569, 'write_down_google_url', NULL, NULL),
(570, 'instagram_link', NULL, NULL),
(571, 'write_down_instagram_url', NULL, NULL),
(572, 'pinterest_link', NULL, NULL),
(573, 'write_down_pinterest_url', NULL, NULL),
(574, 'update_settings', NULL, NULL),
(575, 'upload_banner_image', NULL, NULL),
(576, 'update_light_logo', NULL, NULL),
(577, 'upload_light_logo', NULL, NULL),
(578, 'update_dark_logo', NULL, NULL),
(579, 'upload_dark_logo', NULL, NULL),
(580, 'update_small_logo', NULL, NULL),
(581, 'upload_small_logo', NULL, NULL),
(582, 'upload_favicon', NULL, NULL),
(583, 'logo_updated', NULL, NULL),
(584, 'favicon_updated', NULL, NULL),
(585, 'banner_image_update', NULL, NULL),
(586, 'frontend_settings_updated', NULL, NULL),
(587, 'setup_payment_informations', NULL, NULL),
(588, 'update_system_currency', NULL, NULL),
(589, 'setup_paypal_settings', NULL, NULL),
(590, 'update_paypal_keys', NULL, NULL),
(591, 'setup_stripe_settings', NULL, NULL),
(592, 'test_mode', NULL, NULL),
(593, 'update_stripe_keys', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `lesson`
--

CREATE TABLE `lesson` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `duration` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  `video_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `external_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_added` int(11) DEFAULT NULL,
  `last_modified` int(11) DEFAULT NULL,
  `lesson_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attachment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attachment_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `summary` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 0,
  `video_type_for_mobile_application` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_url_for_mobile_application` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `duration_for_mobile_application` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `lesson`
--

INSERT INTO `lesson` (`id`, `title`, `duration`, `course_id`, `section_id`, `video_type`, `video_url`, `external_url`, `date_added`, `last_modified`, `lesson_type`, `attachment`, `attachment_type`, `summary`, `order`, `video_type_for_mobile_application`, `video_url_for_mobile_application`, `duration_for_mobile_application`) VALUES
(2, 'Bài học 1: Phân biệt tay phải/trái - Kí hiệu FingerMath tay phải, tay trái', '00:15:00', 1, 2, 'html5', 'https://thuvien.superbrain.vn/wl/?id=ySgeeT1x47WYixVie1bRdf7EHt5OtP0M', '', 1585933200, 1587747600, 'video', '', 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=ySgeeT1x47WYixVie1bRdf7EHt5OtP0M', '00:15:00'),
(3, 'Bài học 2: Kí hiệu FingerMath hai chữ số', '00:10:00', 1, 2, 'html5', 'https://thuvien.superbrain.vn/wl/?id=CIeatf8PetetPJmWceWReti1IJzhbsva', '', 1585933200, 1587747600, 'video', '', 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=CIeatf8PetetPJmWceWReti1IJzhbsva', '00:10:00'),
(4, 'Bài học 3: Cộng trừ đơn giản 0-9, cộng trừ đơn giản số tròn chục 0-90', '00:05:00', 1, 3, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '', 1585933200, 1587747600, 'video', '', 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '00:05:00'),
(5, 'Bài học 4: Cộng trừ đơn giản 2 chữ số', '00:10:00', 1, 3, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '', 1585933200, 1587747600, 'video', '', 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '00:10:00'),
(6, 'Bài học 5: Giới thiệu công thức LB+. Công thức Little Buddy +4', '00:10:00', 1, 4, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '', 1585933200, 1587747600, 'video', '', 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '00:10:00'),
(7, 'Bài học 6: Gặp gỡ cùng Buddy', '00:05:00', 1, 4, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '', 1585933200, 1587747600, 'video', '', 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '00:10:00'),
(8, 'Bài học 7: Công thức Little Buddy +3', '00:05:00', 1, 5, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '', 1585933200, 1587747600, 'video', '', 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '00:05:00'),
(9, 'Cùng Buddy khám phá phương pháp Superbrain', '00:20:00', 3, 26, 'html5', 'https://thuvien.superbrain.vn/wl/?id=kKW4vSbezsfU0yue70h4t7D6r7eoPtyu', '', 1585933200, 1587056400, 'video', '', 'url', 'Cùng Buddy khám phá phương pháp Superbrain với thật điều thú vị.', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=kKW4vSbezsfU0yue70h4t7D6r7eoPtyu', '00:00:00'),
(10, 'Em yêu khoa học', '00:10:00', 3, 27, 'html5', 'https://thuvien.superbrain.vn/wl/?id=JShjsb4qYIL7S4pKlguA9cwBIefl2Dw5', '', 1585933200, 1587056400, 'video', '', 'url', 'Có rất nhiều điều thú vị bên ngoài thế giới của chúng ta. Và hôm nay các bạn hãy cùng Buddy khám phá khoa học nhé!', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=JShjsb4qYIL7S4pKlguA9cwBIefl2Dw5', '00:10:00'),
(11, 'Bài học 8: Công thức Little Buddy +2', '00:15:00', 1, 5, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '', 1585933200, 1587747600, 'video', '', 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '00:15:00'),
(12, 'Bài học 9: Công thức Little Buddy +1', '00:15:00', 1, 6, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '', 1585933200, 1587747600, 'video', '', 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '00:15:00'),
(13, 'Bài học 10: Ôn tập công thức Little Buddy +', '00:15:00', 1, 6, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '', 1585933200, 1587747600, 'video', '', 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '00:15:00'),
(14, 'Bài học 11: Giới thiệu công thức Little Buddy- , công thức Little Buddy-4', '00:15:00', 1, 7, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '', 1585933200, 1587747600, 'video', '', 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '00:15:00'),
(15, 'Bài học 12: Công thức Little Buddy -3', '00:15:00', 1, 7, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '', 1585933200, 1587747600, 'video', '', 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '00:15:00'),
(16, 'Bài học 13: Công thức Little Buddy -2', '00:15:00', 1, 8, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '', 1585933200, 1587747600, 'video', '', 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '00:15:00'),
(17, 'Bài học 14: Công thức Little Buddy -1', '00:15:00', 1, 8, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '', 1585933200, 1587747600, 'video', '', 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '00:15:00'),
(18, 'Bài học 15: Ôn tập công thức Little Buddy + -', '00:15:00', 1, 9, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '', 1585933200, 1587747600, 'video', '', 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '00:15:00'),
(19, 'Bài học 16: Giao lưu cùng Buddy', '00:15:00', 1, 9, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '', 1585933200, 1587747600, 'video', '', 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '00:15:00'),
(20, 'Bài học 17: Giới thiệu công thức Big Buddy +, công thức Big Buddy +9', '00:15:00', 1, 10, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '', 1585933200, 1587747600, 'video', '', 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '00:15:00'),
(21, 'Bài học 18: Công thức Little Buddy &amp; Big Buddy +9', '00:15:00', 1, 10, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '', 1585933200, 1587747600, 'video', '', 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '00:15:00'),
(22, 'Bài học 19: Công thức Big Buddy +8', '00:15:00', 1, 11, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '', 1585933200, 1587747600, 'video', '', 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '00:15:00'),
(23, 'Bài học 20: Công thức Little Buddy &amp; Big Buddy +8', '00:15:00', 1, 11, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '', 1585933200, 1587747600, 'video', '', 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '00:10:00'),
(24, 'Bài học 21: Công thức Big Buddy +7', '00:15:00', 1, 12, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '', 1585933200, 1587747600, 'video', '', 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '00:15:00'),
(25, 'Công thức Little Buddy &amp; Big Buddy +7', '00:15:00', 1, 12, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '', 1585933200, 1587747600, 'video', '', 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '00:15:00'),
(27, 'Bài học 23: Công thức Big Buddy +6, Little Buddy &amp; Big Buddy +6', '00:15:00', 1, 13, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '', 1585933200, 1587747600, 'video', '', 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '00:15:00'),
(28, 'Bài học 24: Giao lưu cùng Buddy', '00:15:00', 1, 13, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '', 1585933200, 1587747600, 'video', '', 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=RRToHdc3hq8n0drJyd3hUeAlOHiDfV4t', '00:15:00'),
(29, 'Luyện tập online', NULL, 1, 28, NULL, NULL, 'https://app.superbrain.edu.vn/login', 1586797200, 1586970000, 'other', 'febdcfd54e4545a55efccd1d2c59555e.jpg', 'img', 'Các bé làm bài tập về nhà nhé!', 0, NULL, NULL, NULL),
(32, 'Luyện tập Online', '', 3, 26, '', '', 'https://app.superbrain.edu.vn/', 1586970000, 1587056400, 'other', '12e5c517fda7d60fee7fb1c97928e8e3.jpg', 'img', 'Luyện tập online cùng Buddy nhé các bạn!', 0, '', '', ''),
(33, 'Bài học 1', '00:00:00', 2, 29, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '', 1587056400, NULL, 'video', NULL, 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '00:25:00'),
(34, 'Bài học 2', '00:25:00', 2, 29, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '', 1587056400, NULL, 'video', NULL, 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '00:25:00'),
(35, 'Bài học 3', '00:25:00', 2, 30, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '', 1587056400, NULL, 'video', NULL, 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '00:25:00'),
(36, 'Bài học 4', '00:25:00', 2, 30, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '', 1587056400, NULL, 'video', NULL, 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '00:25:00'),
(37, 'Bài học 5', '00:25:00', 2, 31, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '', 1587056400, NULL, 'video', NULL, 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '00:25:00'),
(38, 'Bài học 6', '00:25:00', 2, 31, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '', 1587056400, NULL, 'video', NULL, 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '00:25:00'),
(39, 'Bài học 7', '00:25:00', 2, 32, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '', 1587056400, NULL, 'video', NULL, 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '00:25:00'),
(40, 'Bài học 8', '00:25:00', 2, 32, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '', 1587056400, NULL, 'video', NULL, 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '00:25:00'),
(41, 'Bài học 9', '00:25:00', 2, 33, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '', 1587056400, NULL, 'video', NULL, 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '00:25:00'),
(42, 'Bài học 10', '00:25:00', 2, 33, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '', 1587056400, NULL, 'video', NULL, 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '00:25:00'),
(43, 'Bài học 11', '00:25:00', 2, 34, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '', 1587056400, NULL, 'video', NULL, 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '00:25:00'),
(44, 'Bài học 12', '00:25:00', 2, 34, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '', 1587056400, NULL, 'video', NULL, 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '00:25:00'),
(45, 'Bài học 13', '00:25:00', 2, 35, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '', 1587056400, NULL, 'video', NULL, 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '00:25:00'),
(46, 'Bài học 14', '00:25:00', 2, 35, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '', 1587056400, NULL, 'video', NULL, 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '00:25:00'),
(47, 'Bài học 15', '00:25:00', 2, 36, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '', 1587056400, NULL, 'video', NULL, 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '00:25:00'),
(48, 'Bài học 16', '00:25:00', 2, 36, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '', 1587056400, NULL, 'video', NULL, 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '00:25:00'),
(49, 'Bài học 17', '00:25:00', 2, 37, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '', 1587056400, NULL, 'video', NULL, 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '00:25:00'),
(50, 'Bài học 18', '00:25:00', 2, 37, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '', 1587056400, NULL, 'video', NULL, 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '00:25:00'),
(51, 'Bài học 19', '00:25:00', 2, 38, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '', 1587056400, NULL, 'video', NULL, 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '00:25:00'),
(52, 'Bài học 20', '00:25:00', 2, 38, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '', 1587056400, NULL, 'video', NULL, 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '00:25:00'),
(53, 'Bài học 21', '00:25:00', 2, 39, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '', 1587056400, NULL, 'video', NULL, 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '00:25:00'),
(54, 'Bài học 22', '00:25:00', 2, 39, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '', 1587056400, NULL, 'video', NULL, 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '00:25:00'),
(55, 'Bài học 23', '00:25:00', 2, 40, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '', 1587056400, NULL, 'video', NULL, 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '00:25:00'),
(56, 'Bài học 24', '00:25:00', 2, 40, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '', 1587056400, NULL, 'video', NULL, 'url', '', 0, 'html5', 'https://thuvien.superbrain.vn/wl/?id=3lEiCX2jVgLSVG8i6ODMNCcx15XfY2F6', '00:25:00'),
(57, 'Hướng dẫn sử dụng phần mềm luyện tập online', NULL, 3, 26, NULL, NULL, '', 1587229200, NULL, 'other', '73be8ce26bba0aadb28fe6860d858a7d.pdf', 'pdf', '', 0, NULL, NULL, NULL),
(58, 'Hướng dẫn sử dụng phần mềm học trực tuyến cùng giáo viên - Zoom', NULL, 3, 26, NULL, NULL, '', 1587229200, NULL, 'other', 'e3c540e05830478e1534bf6bc4cbe881.pdf', 'pdf', '', 0, NULL, NULL, NULL),
(59, 'Hướng dẫn sử dụng phần mềm luyện tập online', NULL, 1, 28, NULL, NULL, '', 1587747600, NULL, 'other', '4cf1524d68d57ca91b8cc122ff5c4341.pdf', 'pdf', '', 0, NULL, NULL, NULL),
(60, 'Hướng dẫn sử dụng phần mềm học trực tuyến cùng giáo viên - Zoom', NULL, 1, 28, NULL, NULL, '', 1587747600, NULL, 'other', '75d8454f204a453ef2034bfa055911d1.pdf', 'pdf', '', 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `message`
--

CREATE TABLE `message` (
  `message_id` int(11) NOT NULL,
  `message_thread_code` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `sender` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `read_status` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `message`
--

INSERT INTO `message` (`message_id`, `message_thread_code`, `message`, `sender`, `timestamp`, `read_status`) VALUES
(1, 'e64ce79721a0b8f', 'Xin chào', '8', '1585998630', NULL),
(2, 'e64ce79721a0b8f', 'Xin chào', '3', '1585998648', 1),
(3, '0faee863f81b54d', 'Chào ad....giúp tôi với!', '3', '1586764364', NULL),
(4, '2fc224e51306055', 'Xin chào', '3', '1587000694', NULL),
(5, '2f941631c8d8625', 'Chào bạn!  Mình rất hài lòng về chương trình nhé ạ!', '12', '1587116837', NULL),
(6, '8b225ac1f2446ef', 'Chúng tôi muốn biết là Hạnh là bên bộ phận này để chúng tôi cần liên hệ với ai về chất lượng của bé ạ.', '12', '1587116883', NULL),
(7, '247c98afca4fb25', 'alo alo\r\n', '59', '1587463092', NULL),
(8, '247c98afca4fb25', 'có ai đọc được tin nhắn không ah\r\n', '59', '1587463112', NULL),
(9, '0faee863f81b54d', 'Cô ơi, em cần cô giúp đỡ', '3', '1587653349', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `message_thread`
--

CREATE TABLE `message_thread` (
  `message_thread_id` int(11) NOT NULL,
  `message_thread_code` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `sender` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `receiver` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `last_message_timestamp` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `message_thread`
--

INSERT INTO `message_thread` (`message_thread_id`, `message_thread_code`, `sender`, `receiver`, `last_message_timestamp`) VALUES
(1, 'e64ce79721a0b8f', '8', '3', NULL),
(2, '0faee863f81b54d', '3', '6', NULL),
(3, '2fc224e51306055', '3', '1', NULL),
(4, '2f941631c8d8625', '12', '1', NULL),
(5, '8b225ac1f2446ef', '12', '6', NULL),
(6, '247c98afca4fb25', '59', '1', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `payment`
--

CREATE TABLE `payment` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `payment_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `date_added` int(11) DEFAULT NULL,
  `last_modified` int(11) DEFAULT NULL,
  `admin_revenue` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instructor_revenue` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instructor_payment_status` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `question`
--

CREATE TABLE `question` (
  `id` int(11) UNSIGNED NOT NULL,
  `quiz_id` int(11) DEFAULT NULL,
  `title` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `number_of_options` int(11) DEFAULT NULL,
  `options` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `correct_answers` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `question`
--

INSERT INTO `question` (`id`, `quiz_id`, `title`, `type`, `number_of_options`, `options`, `correct_answers`, `order`) VALUES
(1, 1, 'Câu 1', 'multiple_choice', 5, '[\"Option 1\",\"Option 2\",\"Option 3\",\"Option 4\",\"Option 5\"]', '[\"2\"]', 0),
(2, 1, 'Câu 2', 'multiple_choice', 3, '[\"\\u0110\\u00e1p \\u00e1n 1\",\"\\u0110\\u00e1p \\u00e1n 2\",\"\\u0110\\u00e1p \\u00e1n 3\"]', '[\"2\"]', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `rating`
--

CREATE TABLE `rating` (
  `id` int(11) UNSIGNED NOT NULL,
  `rating` double DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ratable_id` int(11) DEFAULT NULL,
  `ratable_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` int(11) DEFAULT NULL,
  `last_modified` int(11) DEFAULT NULL,
  `review` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `rating`
--

INSERT INTO `rating` (`id`, `rating`, `user_id`, `ratable_id`, `ratable_type`, `date_added`, `last_modified`, `review`) VALUES
(1, NULL, NULL, NULL, 'course', 1587229200, NULL, NULL),
(2, 5, 3, 3, 'course', 1587142800, NULL, 'Bé học rất vui, tôi rất thích khóa học này'),
(3, 5, 13, 3, 'course', 1587142800, NULL, ''),
(4, 5, 13, 1, 'course', 1587142800, NULL, 'Rất thích chương trình, bé đã tiến bộ rất nhiều. Bé hứng thú học tập và thích nhân vật Buddy lắm'),
(5, 5, 13, 2, 'course', 1587142800, NULL, ''),
(6, 5, 3, 1, 'course', 1587142800, NULL, ''),
(7, 5, 12, 2, 'course', 1587142800, NULL, 'Rất ok'),
(8, 5, 12, 3, 'course', 1587142800, NULL, 'Rất tốt');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `role`
--

CREATE TABLE `role` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` int(11) DEFAULT NULL,
  `last_modified` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `role`
--

INSERT INTO `role` (`id`, `name`, `date_added`, `last_modified`) VALUES
(1, 'Admin', 1234567890, 1234567890),
(2, 'User', 1234567890, 1234567890);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `section`
--

CREATE TABLE `section` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `section`
--

INSERT INTO `section` (`id`, `title`, `course_id`, `order`) VALUES
(2, 'Tuần 1', 1, 1),
(3, 'Tuần 2', 1, 2),
(4, 'Tuần 3', 1, 3),
(5, 'Tuần 4', 1, 4),
(6, 'Tuần 5', 1, 5),
(7, 'Tuần 6', 1, 6),
(8, 'Tuần 7', 1, 7),
(9, 'Tuần 8', 1, 8),
(10, 'Buổi 9', 1, 9),
(11, 'Buổi 10', 1, 10),
(12, 'Buổi 11', 1, 11),
(13, 'Buổi 12', 1, 12),
(26, 'Trải nghiệm cùng Superbrain', 3, 0),
(27, 'Em yêu khoa học', 3, 0),
(28, 'Hướng dẫn về bài học Superbrain', 1, 0),
(29, 'Tuần 1', 2, 1),
(30, 'Tuần 2', 2, 2),
(31, 'Tuần 3', 2, 3),
(32, 'Tuần 4', 2, 4),
(33, 'Tuần 5', 2, 5),
(34, 'Tuần 6', 2, 6),
(35, 'Tuần 7', 2, 7),
(36, 'Tuần 8', 2, 8),
(37, 'Tuần 9', 2, 9),
(38, 'Tuần 10', 2, 10),
(39, 'Tuần 11', 2, 11),
(40, 'Tuần 12', 2, 12);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `settings`
--

CREATE TABLE `settings` (
  `id` int(11) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `settings`
--

INSERT INTO `settings` (`id`, `key`, `value`) VALUES
(1, 'language', 'vietnamese'),
(2, 'system_name', 'Superbrain Online - Khơi Dậy Tiềm Năng'),
(3, 'system_title', 'Superbrainonline.com'),
(4, 'system_email', 'Online@superbrain.edu.vn'),
(5, 'address', 'Headquarter: Số 37 Đường 19, P.Bình An, Quận 2, TP.HCM'),
(6, 'phone', '090 639 1800 / 090 389 1800'),
(7, 'purchase_code', '184e4d60-2f3a-4a09-9b66-3b70bbc16679'),
(8, 'paypal', '[{\"active\":\"0\",\"mode\":\"sandbox\",\"sandbox_client_id\":\"sandbox-client-id\",\"sandbox_secret_key\":\"sandbox-secret-key\",\"production_client_id\":\"product-client-id\",\"production_secret_key\":\"production-secret\"}]'),
(9, 'stripe_keys', '[{\"active\":\"0\",\"testmode\":\"off\",\"public_key\":\"pk_test_xxxxxxxxxxxxxxxxxxxxxxxx\",\"secret_key\":\"sk_test_xxxxxxxxxxxxxxxxxxxxxxxx\",\"public_live_key\":\"pk_live_xxxxxxxxxxxxxxxxxxxxxxxx\",\"secret_live_key\":\"sk_live_xxxxxxxxxxxxxxxxxxxxxxxx\"}]'),
(10, 'youtube_api_key', 'AIzaSyA44sfynFR5zR_iD--NcKpxCxBjFqn2M6Q'),
(11, 'vimeo_api_key', 'vimeo-api-key'),
(12, 'slogan', 'Khơi Dậy Tiềm Năng'),
(13, 'text_align', NULL),
(14, 'allow_instructor', '0'),
(15, 'instructor_revenue', '70'),
(16, 'system_currency', 'VND'),
(17, 'paypal_currency', 'USD'),
(18, 'stripe_currency', 'VND'),
(19, 'author', 'Superbrain'),
(20, 'currency_position', 'right'),
(21, 'website_description', 'Superbrain Online - Phương pháp giúp trẻ Tập trung, Tự giác học tập tại nhà'),
(22, 'website_keywords', 'LMS,Learning Management System,Khơi Dậy Tiềm Năng,Toán trí tuệ online,Superbrain online'),
(23, 'footer_text', 'Superbrain - Khơi Dậy Tiềm Năng'),
(24, 'footer_link', 'https://superbrain.edu.vn/'),
(25, 'protocol', 'smtp'),
(26, 'smtp_host', 'ssl://smtp.googlemail.com'),
(27, 'smtp_port', '465'),
(28, 'smtp_user', 'superbraingroup@gmail.com'),
(29, 'smtp_pass', 'SPB@2020'),
(30, 'version', '3.3.1 - Đã tùy chỉnh bởi Superbrain'),
(31, 'student_email_verification', 'disable');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tag`
--

CREATE TABLE `tag` (
  `id` int(11) UNSIGNED NOT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tagable_id` int(11) DEFAULT NULL,
  `tagable_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` int(11) DEFAULT NULL,
  `last_modified` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `social_links` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `biography` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `date_added` int(11) DEFAULT NULL,
  `last_modified` int(11) DEFAULT NULL,
  `watch_history` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `wishlist` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `paypal_keys` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `stripe_keys` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `verification_code` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `social_links`, `biography`, `role_id`, `date_added`, `last_modified`, `watch_history`, `wishlist`, `title`, `paypal_keys`, `stripe_keys`, `verification_code`, `status`) VALUES
(1, 'Admin', 'Superbrain', 'admin@superbrain.edu.vn', '1084d2daf2b0c28abc2b4efd11fffd4fc1b543e3', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', NULL, 1, 1584765763, NULL, '[{\"lesson_id\":\"29\",\"progress\":\"1\"}]', '[]', NULL, '[{\"production_client_id\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', '44ac41487d7be3dc3a10201daa089aaa', 1),
(2, 'Tuan', 'Nguyen', 'khachhang@superbrain.vn', '93f281ad4c8f8430bef5a61260d83bc7874426f1', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', NULL, 2, 1584765763, NULL, '[]', '[]', NULL, '[{\"production_client_id\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', '44ac41487d7be3dc3a10201daa089aaa', 1),
(3, 'Thiên Ân', 'Hoàng', 'hocvien@superbrain.edu.vn', '1084d2daf2b0c28abc2b4efd11fffd4fc1b543e3', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1585983229, 1587178221, '[{\"lesson_id\":\"9\",\"progress\":\"1\"},{\"lesson_id\":\"2\",\"progress\":\"1.886803\"},{\"lesson_id\":\"3\",\"progress\":\"0\"},{\"lesson_id\":\"29\",\"progress\":\"0\"},{\"lesson_id\":\"4\",\"progress\":\"0\"},{\"lesson_id\":\"5\",\"progress\":\"0\"},{\"lesson_id\":\"6\",\"progress\":\"0\"},{\"lesson_id\":\"7\",\"progress\":\"0\"},{\"lesson_id\":\"8\",\"progress\":\"0\"},{\"lesson_id\":\"11\",\"progress\":\"0\"},{\"lesson_id\":\"32\",\"progress\":\"0\"},{\"lesson_id\":\"10\",\"progress\":\"1\"},{\"lesson_id\":\"57\",\"progress\":\"0\"}]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(5, 'Tuan', 'NA', 'tuanbkc@gmail.com', 'cd320b1d28fbc91cb9fabd8ce116bfd76c44753d', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', NULL, 2, 1585985329, NULL, '[]', '[]', NULL, '[{\"production_client_id\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', '5b66fa45cb4f9c1aad8c5448d21718e3', 1),
(6, 'Hạnh', 'Nguyễn', 'nguyenphamhonghanh@superbrain.edu.vn', 'b37cce91fa7db92dbf5f4b6e5d68ae8a86e4b924', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', NULL, 1, 1584765763, NULL, '[]', '[]', NULL, '[{\"production_client_id\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', '44ac41487d7be3dc3a10201daa089aaa', 1),
(7, 'Giáo viên', 'Tài khoản', 'namth05063@gmail.com', '78613a0688057054283a64a192c30b25e4a8d2ef', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', NULL, 2, 1585987058, NULL, '[]', '[]', NULL, '[{\"production_client_id\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', '873b3717ebf48b84b31ae41803ba97a7', 1),
(8, 'Web', 'Management', 'web@superbrain.edu.vn', '1084d2daf2b0c28abc2b4efd11fffd4fc1b543e3', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', NULL, 1, 1584765763, NULL, '[]', '[]', NULL, '[{\"production_client_id\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', '44ac41487d7be3dc3a10201daa089aaa', 1),
(9, 'Hồng Hạnh', 'Nguyễn Phạm', 'phamhanh17576@gmail.com', 'bde73bb56ca25e65ca1a6371e9e55db0b9e91d7d', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', NULL, 2, 1586147688, NULL, '[]', '[]', NULL, '[{\"production_client_id\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', '5a77d99864f5a08b3545030ece2e7d00', 1),
(10, 'Cơ Sở', 'Superbrain', 'Coso0123456789', 'b7046b5b69d25c252030240b8a6d7817b63a5a97', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1585983229, 1587019374, '[{\"lesson_id\":\"9\",\"progress\":\"0\"},{\"lesson_id\":\"2\",\"progress\":\"1\"},{\"lesson_id\":\"3\",\"progress\":\"0\"},{\"lesson_id\":\"29\",\"progress\":\"1\"},{\"lesson_id\":\"4\",\"progress\":\"1\"}]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(11, 'hoang', 'bon', 'hoang0938482332', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587089545, NULL, '[{\"lesson_id\":\"9\",\"progress\":\"1\"},{\"lesson_id\":\"32\",\"progress\":\"1\"},{\"lesson_id\":\"57\",\"progress\":\"1\"},{\"lesson_id\":\"58\",\"progress\":\"1\"}]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(12, 'Trúc Đoan', ' ', 'trucdoan0909907311', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587091764, 1587179170, '[{\"lesson_id\":\"9\",\"progress\":\"0\"}]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(13, 'Linh', 'Nguyễn Thị', 'Hon0326446488', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587115124, 1587115220, '[{\"lesson_id\":\"10\",\"progress\":\"1\"},{\"lesson_id\":\"9\",\"progress\":\"1\"}]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(14, 'Bảo', 'Nguyễn Hữu Thiên', 'Bao0989113316', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587198730, NULL, '[]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(15, 'Khuyên', 'Mai Luân ', 'Khuyen0913421113', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587199212, NULL, '[{\"lesson_id\":\"32\",\"progress\":\"1\"},{\"lesson_id\":\"57\",\"progress\":\"1\"},{\"lesson_id\":\"58\",\"progress\":\"1\"}]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(16, 'Vinh', 'Chung Trí ', 'Vinh0913421113', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587199302, NULL, '[{\"lesson_id\":\"10\",\"progress\":\"1\"}]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(17, 'PHI', 'NGUYỄN BÙI VÂN ', 'phi0938374528', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587199696, NULL, '[{\"lesson_id\":\"10\",\"progress\":\"1\"},{\"lesson_id\":\"32\",\"progress\":\"0\"}]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(18, 'phúc', 'lê tấn ', 'phuc0903521886', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587200061, NULL, '[]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(19, 'Trian', ' ', 'Trian0935330909', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587200932, NULL, '[{\"lesson_id\":\"32\",\"progress\":\"1\"}]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(20, 'Linh', ' Nguyễn', 'Linh0326446488', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587201379, NULL, '[]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(21, 'Huy', 'Trần Lưu Quốc ', 'huy0976106810', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587203940, 1587345140, '[{\"lesson_id\":\"9\",\"progress\":\"1\"},{\"lesson_id\":\"32\",\"progress\":\"1\"},{\"lesson_id\":\"57\",\"progress\":\"1\"},{\"lesson_id\":\"58\",\"progress\":\"1\"},{\"lesson_id\":\"10\",\"progress\":\"1\"}]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(22, 'Nhiên', 'Trần An ', 'Nhien0888233135', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587205500, NULL, '[]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(23, 'Khải', 'Trần Quang', 'Khai0917779236', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587215064, 1587215190, '[{\"lesson_id\":\"32\",\"progress\":\"1\"},{\"lesson_id\":\"9\",\"progress\":\"1\"}]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(24, 'Cúc', 'Phương', 'cuc0984419619', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587221895, NULL, '[{\"lesson_id\":\"10\",\"progress\":\"1\"}]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(25, 'Abc', ' ', 'abc012345678', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587222073, NULL, '[]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(26, 'Hưng', 'Việt', 'Hung0938556439', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587222850, NULL, '[{\"lesson_id\":\"9\",\"progress\":\"1\"},{\"lesson_id\":\"57\",\"progress\":\"1\"},{\"lesson_id\":\"58\",\"progress\":\"1\"},{\"lesson_id\":\"32\",\"progress\":\"1\"}]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(27, 'Kiên', 'Nguyễn Trung', 'Kien0989623119', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587222929, NULL, '[]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(28, 'Hiếu', 'Nguyễn Lộc', 'Hieu0919934357', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587223156, NULL, '[{\"lesson_id\":\"10\",\"progress\":\"1\"},{\"lesson_id\":\"9\",\"progress\":\"1\"}]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(29, NULL, NULL, NULL, 'da39a3ee5e6b4b0d3255bfef95601890afd80709', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', NULL, 2, 1587247490, NULL, '[]', '[]', NULL, '[{\"production_client_id\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', 'ef4c0ba724b688ffdb3f62ffc43751c2', 1),
(30, 'Linh', 'Nguyễn Vũ Gia', 'linh0933118039', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587267010, NULL, '[{\"lesson_id\":\"32\",\"progress\":\"1\"},{\"lesson_id\":\"10\",\"progress\":\"0\"},{\"lesson_id\":\"9\",\"progress\":\"1\"},{\"lesson_id\":\"57\",\"progress\":\"1\"},{\"lesson_id\":\"58\",\"progress\":\"0\"}]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(31, 'Hà', 'Lê Thị Thu', 'ha0987386747', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587267093, NULL, '[{\"lesson_id\":\"9\",\"progress\":\"1\"}]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(32, 'Mai', 'Đoàn Thị Thanh', 'mai0968933037', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587267358, NULL, '[]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(33, 'Đan', 'Bùi Thị Linh', 'dan0975867716', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587267484, NULL, '[{\"lesson_id\":\"9\",\"progress\":\"1\"}]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(34, 'Vinh', 'Nguyễn Phú', 'vinh0935037997', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587267541, 1587284559, '[]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(35, 'Anh', 'Linh', 'Anh0384450296', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587267966, NULL, '[{\"lesson_id\":\"32\",\"progress\":\"1\"},{\"lesson_id\":\"9\",\"progress\":\"1\"}]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(36, 'Nguyệt', 'Trần Thị Ánh', 'Nguyet0382651880', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587268326, NULL, '[]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(37, 'Ngọc', 'Võ Trần Bảo', 'Ngoc0943888085', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587268406, NULL, '[]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(38, 'Anh', 'Trần Minh', 'Anh0988365007', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587268548, NULL, '[]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(39, 'Bách', 'Trần Nghi', 'Bach0961547424', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587269050, NULL, '[]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(40, 'Hoà', 'Trần Thị Nhị', 'Hoa0961547424', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587269116, NULL, '[{\"lesson_id\":\"9\",\"progress\":\"791.484489\"}]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(41, 'Ngọc', 'Võ Trần Bảo', 'Baongoc0943888085', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587269872, 1587270079, '[{\"lesson_id\":\"9\",\"progress\":\"739.04471\"},{\"lesson_id\":\"10\",\"progress\":\"1\"}]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(42, 'Vy', 'Khánh', 'Vy0899795237', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587276345, NULL, '[{\"lesson_id\":\"58\",\"progress\":\"0\"},{\"lesson_id\":\"32\",\"progress\":\"1\"},{\"lesson_id\":\"10\",\"progress\":\"1\"},{\"lesson_id\":\"9\",\"progress\":\"0\"}]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(43, 'Nhiên', 'Nguyễn Ngọc An', 'Nhien0939797286', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587276416, 1587276494, '[{\"lesson_id\":\"32\",\"progress\":\"1\"},{\"lesson_id\":\"10\",\"progress\":\"1\"},{\"lesson_id\":\"9\",\"progress\":\"1\"}]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(44, 'Hoang', 'Mark', 'hoang4034042888', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587277304, NULL, '[]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(45, 'Mark', 'Hoang', 'mark0938482332', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587278517, NULL, '[]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(46, 'Viên', 'Giáo', 'Vien0901628808', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587279079, NULL, '[]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(47, 'Thùy', 'Hoàng Ngọc', 'Thuy0972493368', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587284398, NULL, '[]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(48, 'Anh', 'Nguyễn Thị Phương', 'Anh0848368225', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587284473, NULL, '[]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(49, 'Le', ' ', 'Le0969915735', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587284722, NULL, '[{\"lesson_id\":\"32\",\"progress\":\"1\"},{\"lesson_id\":\"57\",\"progress\":\"1\"},{\"lesson_id\":\"58\",\"progress\":\"1\"}]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(50, 'Quân', 'Nguyễn Hữu', 'Quan0982387119', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587284855, NULL, '[{\"lesson_id\":\"32\",\"progress\":\"1\"},{\"lesson_id\":\"58\",\"progress\":\"0\"}]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(51, 'Tuyến', 'Hoàng Kim', 'Tuyen0946780844', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587285728, NULL, '[]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(52, 'Yến', 'Nguyễn Vương Xuân', 'Yen0916731105', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587285811, NULL, '[{\"lesson_id\":\"32\",\"progress\":\"1\"},{\"lesson_id\":\"9\",\"progress\":\"1\"},{\"lesson_id\":\"10\",\"progress\":\"1\"}]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(53, 'Oanh', 'Trần Đỗ Vy', 'Oanh0941733557', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587287685, NULL, '[]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(55, 'Chân', 'Hoàng Thị Lệ', 'Chan0906494173', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587288372, 1587344756, '[{\"lesson_id\":\"9\",\"progress\":\"0\"},{\"lesson_id\":\"32\",\"progress\":\"1\"}]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(56, 'Khuê', 'Hồ Việt', 'Khue0901087111', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587351168, NULL, '[{\"lesson_id\":\"9\",\"progress\":\"1\"},{\"lesson_id\":\"32\",\"progress\":\"1\"},{\"lesson_id\":\"57\",\"progress\":\"1\"},{\"lesson_id\":\"10\",\"progress\":\"1\"},{\"lesson_id\":\"58\",\"progress\":\"1\"}]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(57, 'Chung', 'Trương Ngọc', 'Chung0397168702', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587365178, NULL, '[]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(58, 'Bao', 'Nguyen Thien', 'Bao0989788463', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587437137, NULL, '[{\"lesson_id\":\"9\",\"progress\":\"0\"}]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(59, 'Khánh', 'Nguyễn Phạm Kim', 'khanh0911089888', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587462169, NULL, '[]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(60, 'Phuong', 'Vo Hoang Nam', 'Phuong0937832222', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587543173, NULL, '[]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(61, 'Thư', 'Nguyễn Ngọc Anh', 'Thu0988009655', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587545625, NULL, '[{\"lesson_id\":\"9\",\"progress\":\"1\"},{\"lesson_id\":\"32\",\"progress\":\"1\"},{\"lesson_id\":\"57\",\"progress\":\"1\"},{\"lesson_id\":\"58\",\"progress\":\"1\"},{\"lesson_id\":\"10\",\"progress\":\"1\"}]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(62, 'Khoa', 'Mai Tiến', 'Khoa0989105113', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587548881, NULL, '[{\"lesson_id\":\"9\",\"progress\":\"1032.152411902\"},{\"lesson_id\":\"10\",\"progress\":\"245.706726\"}]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(63, 'Vân', ' ', 'van0938890142', 'dbdcf35df2661f6f27f22a30bcd9333a51c4a29a', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1587693608, NULL, '[]', '[]', NULL, '[{\"production_client_id\":\"\",\"production_secret_key\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(64, 'Dev', 'Superbrain', 'dev@superbrain.edu.vn', '28b11e0e3f13623283765474f48e3b837023836c', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', NULL, 1, 1584765763, NULL, '[{\"lesson_id\":\"29\",\"progress\":\"1\"}]', '[]', NULL, '[{\"production_client_id\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', '44ac41487d7be3dc3a10201daa089aaa', 1);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `addons`
--
ALTER TABLE `addons`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `enrol`
--
ALTER TABLE `enrol`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `frontend_settings`
--
ALTER TABLE `frontend_settings`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`phrase_id`);

--
-- Chỉ mục cho bảng `lesson`
--
ALTER TABLE `lesson`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`message_id`);

--
-- Chỉ mục cho bảng `message_thread`
--
ALTER TABLE `message_thread`
  ADD PRIMARY KEY (`message_thread_id`);

--
-- Chỉ mục cho bảng `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `section`
--
ALTER TABLE `section`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `addons`
--
ALTER TABLE `addons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT cho bảng `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `course`
--
ALTER TABLE `course`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;

--
-- AUTO_INCREMENT cho bảng `enrol`
--
ALTER TABLE `enrol`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT cho bảng `frontend_settings`
--
ALTER TABLE `frontend_settings`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT cho bảng `language`
--
ALTER TABLE `language`
  MODIFY `phrase_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=594;

--
-- AUTO_INCREMENT cho bảng `lesson`
--
ALTER TABLE `lesson`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT cho bảng `message`
--
ALTER TABLE `message`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `message_thread`
--
ALTER TABLE `message_thread`
  MODIFY `message_thread_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `question`
--
ALTER TABLE `question`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `rating`
--
ALTER TABLE `rating`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `section`
--
ALTER TABLE `section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT cho bảng `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT cho bảng `tag`
--
ALTER TABLE `tag`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
