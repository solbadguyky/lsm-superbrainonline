<div class="form-group row mb-3">
    <label class="col-md-2 col-form-label"
        for="position"><?php echo get_phrase('course_position'); ?></label>
    <div class="col-md-10">
        <input type="number" class="form-control" id="position" name="position"
            placeholder="<?php echo get_phrase('course_position_hint'); ?>" value="<?php echo $course_details['position'] ? $course_details['position'] : ''; ?>">
    </div>
</div>