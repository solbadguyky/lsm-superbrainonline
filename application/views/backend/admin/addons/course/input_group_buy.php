<?php ?>

<div class="form-group row mb-3">
    <label class="col-md-2 col-form-label"
        for="buy_url"><?php echo get_phrase('course_external_buy_url'); ?></label>
    <div class="col-md-10">
        <input type="text" class="form-control" id="buy_url" name="buy_url"
            placeholder="<?php echo get_phrase('course_external_buy_url_hint'); ?>" value="<?php echo $course_details['buy_url'] ? $course_details['buy_url'] : ''; ?>">
    </div>
</div>