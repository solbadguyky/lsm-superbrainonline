<div class="col-lg-4">
    <div class="course-sidebar natural">
        <?php if ($course_details['video_url'] != ""): ?>
        <div class="preview-video-box">
            <a data-toggle="modal" data-target="#CoursePreviewModal">
                <img src="<?php echo $this->crud_model->get_course_thumbnail_url($course_details['id']); ?>" alt=""
                    class="img-fluid">
                <span class="preview-text"><?php echo get_phrase('preview_this_course'); ?></span>
                <span class="play-btn"></span>
            </a>
        </div>
        <?php endif; ?>
        <div class="course-sidebar-text-box">
            <div class="price">
                <?php if ($course_details['is_free_course'] == 1): ?>
                <span class="current-price"><span class="current-price"><?php echo get_phrase('free'); ?></span></span>
                <?php else: ?>
                <?php if ($course_details['discount_flag'] == 1): ?>
                <span class="current-price"><span
                        class="current-price"><?php echo currency($course_details['discounted_price']); ?></span></span>
                <span class="original-price"><?php echo currency($course_details['price']) ?></span>
                <input type="hidden" id="total_price_of_checking_out"
                    value="<?php echo currency($course_details['discounted_price']); ?>">
                <?php else: ?>
                <span class="current-price"><span
                        class="current-price"><?php echo currency($course_details['price']); ?></span></span>
                <input type="hidden" id="total_price_of_checking_out"
                    value="<?php echo currency($course_details['price']); ?>">
                <?php endif; ?>
                <?php endif; ?>
            </div>

            <?php if(is_purchased($course_details['id'])) :?>
            <div class="already_purchased">
                <a href="<?php echo site_url('home/my_courses'); ?>"><?php echo get_phrase('already_purchased'); ?></a>
            </div>
            <?php else: ?>
            <?php if ($course_details['is_free_course'] == 1): ?>
            <div class="buy-btns">
                <?php if ($this->session->userdata('user_login') != 1): ?>
                <a href="#" class="btn btn-buy-now"
                    onclick="handleEnrolledButton()"><?php echo get_phrase('get_enrolled'); ?></a>
                <?php else: ?>
                <a href="<?php echo site_url('home/get_enrolled_to_free_course/'.$course_details['id']); ?>"
                    class="btn btn-buy-now"><?php echo get_phrase('get_enrolled'); ?></a>
                <?php endif; ?>
            </div>
            <?php else: ?>
            <div class="buy-btns">
                <?php if($course_details['buy_url']){ ?>
                    <a href="<?php echo $course_details['buy_url']; ?>" class="btn btn-buy-now" id="course_<?php echo $course_details['id']; ?>"
                    target="_blank"><?php echo get_phrase('buy_now'); ?></a>
                <?php } else {?>
                    <a href="javascript::" class="btn btn-buy-now" id="course_<?php echo $course_details['id']; ?>"
                    onclick="handleBuyNow(this)"><?php echo get_phrase('buy_now'); ?></a>
                    <?php if (in_array($course_details['id'], $this->session->userdata('cart_items'))): ?>
                    <button class="btn btn-add-cart addedToCart" type="button" id="<?php echo $course_details['id']; ?>"
                        onclick="handleCartItems(this)"><?php echo get_phrase('added_to_cart'); ?></button>
                    <?php else: ?>
                    <button class="btn btn-add-cart" type="button" id="<?php echo $course_details['id']; ?>"
                        onclick="handleCartItems(this)"><?php echo get_phrase('add_to_cart'); ?></button>
                    <?php endif; ?>
                <?php } ?>
               
            </div>
            <?php endif; ?>
            <?php endif; ?>


            <div class="includes">
                <div class="title"><b><?php echo get_phrase('includes'); ?>:</b></div>
                <ul>
                    <li><i class="far fa-file-video"></i>
                        <?php
            echo $this->crud_model->get_total_duration_of_lesson_by_course_id($course_details['id']).' '.get_phrase('on_demand_videos');
            ?>
                    </li>
                    <li><i
                            class="far fa-file"></i><?php echo $this->crud_model->get_lessons('course', $course_details['id'])->num_rows().' '.get_phrase('lessons'); ?>
                    </li>
                    <li><i class="far fa-compass"></i><?php echo get_phrase('full_lifetime_access'); ?></li>
                    <li><i class="fas fa-mobile-alt"></i><?php echo get_phrase('access_on_mobile_and_tv'); ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
</div>