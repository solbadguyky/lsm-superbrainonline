<section class="course-carousel-area section-testimonial-courses">
    <div class="container-lg">
        <div class="row">
            <div class="col">
                <h2 class="course-carousel-title"><?php echo get_phrase('testimonial_courses_heading'); ?></h2>
                <div class="course-carousel">
                    <?php
                    $testimonials = array(
                        [
                            "src" => " https://thuvien.superbrain.vn/wl/?id=T0IPKO9l9HYLunBqVmfUwNWJWRl6ujdF"
                        ],
                        [
                            "src" => "https://thuvien.superbrain.vn/wl/?id=1FrO6QB3vYh2Znuk2QTjGH32WfjqqTLL"
                        ],
                        [
                            "src" => "https://thuvien.superbrain.vn/wl/?id=X4qC59YxxoNhtcJseZ6qRmUwtyj79gF4"
                        ],
                        [
                            "src" => "https://thuvien.superbrain.vn/wl/?id=fKE3voXMLenXNbaJ3u88IwNngVX0Mf1i"
                        ],
                        [
                            "src" => "https://thuvien.superbrain.vn/wl/?id=ORenrh7sY3GtQJQC7Jj04z2H6FgTeOMg"
                        ],
                        [
                            "src" => "https://thuvien.superbrain.vn/wl/?id=qnX7Di2SNYOX4uqoR8129XAgUCh5bPd0"
                        ],
                        [
                            "src" => "https://thuvien.superbrain.vn/wl/?id=TXfnSbcNnk6plEAMfCUH5rrpQvn2H74K"
                        ],
                        [
                            "src" => "https://thuvien.superbrain.vn/wl/?id=VYPRr9Z1gbmQ2FIQ78yYesYXcHZGudgF"
                        ],
                        [
                            "src" => "https://thuvien.superbrain.vn/wl/?id=uaNrRKAXtXhs45yyUL0SY9kPGxoikyY8"
                        ],
                        [
                            "src" => "https://thuvien.superbrain.vn/wl/?id=GeolefP9QRuKbfASOBcmKc8e0C338D6t"
                        ],
                    );
                    foreach ($testimonials as $testimonial):?>
                    <div class="course-box-wrap">
                        <div class="course-box">
                            <img class="testimonial-course-img" src="<?php echo  $testimonial['src']; ?>">
                        </div>
                    </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
</div>
</section>