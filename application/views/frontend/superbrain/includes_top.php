<?php $version = microtime(true); ?>
<link rel="favicon" href="<?php echo base_url().'assets/frontend/superbrain/img/icons/favicon.ico' ?>">
<link rel="apple-touch-icon" href="<?php echo base_url().'assets/frontend/superbrain/img/icons/icon.png'; ?>">
<link rel="stylesheet" href="<?php echo base_url().'assets/frontend/superbrain/css/jquery.webui-popover.min.css?ver= ' . $version ; ?>">
<link rel="stylesheet" href="<?php echo base_url().'assets/frontend/superbrain/css/select2.min.css?ver= ' . $version ; ?>">
<link rel="stylesheet" href="<?php echo base_url().'assets/frontend/superbrain/css/slick.css?ver= ' . $version ; ?>">
<link rel="stylesheet" href="<?php echo base_url().'assets/frontend/superbrain/css/slick-theme.css?ver= ' . $version ; ?>">
<!-- font awesome 5 -->
<link rel="stylesheet" href="<?php echo base_url().'assets/frontend/superbrain/css/fontawesome-all.min.css?ver= ' . $version ; ?>">

<link rel="stylesheet" href="<?php echo base_url().'assets/frontend/superbrain/css/bootstrap.min.css?ver= ' . $version ; ?>">
<link rel="stylesheet" href="<?php echo base_url().'assets/frontend/superbrain/css/bootstrap-tagsinput.css?ver= ' . $version ; ?>">
<link rel="stylesheet" href="<?php echo base_url().'assets/frontend/superbrain/css/main.css?ver= ' . $version ; ?>">
<link rel="stylesheet" href="<?php echo base_url().'assets/frontend/superbrain/css/responsive.css?ver= ' . $version ; ?>">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url().'assets/global/toastr/toastr.css?ver= ' . $version  ?>">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/nestable2/1.6.0/jquery.nestable.min.css" />
<script src="<?php echo base_url('assets/backend/js/jquery-3.3.1.min.js'); ?>"></script>
