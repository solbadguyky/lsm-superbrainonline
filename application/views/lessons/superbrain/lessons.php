<?php
$course_details = $this->crud_model->get_course_by_id($course_id)->row_array();
$my_course_url = strtolower($this->session->userdata('role')) == 'user' ? site_url('home/my_courses') : 'javascript::';
$course_details_url = site_url("home/course/".slugify($course_details['title'])."/".$course_id);
?>
<div class="container-fluid course_container">
    <!-- Top bar -->
    <div class="top-bar-wrapper">
        <div class="row">
            <div class="col-lg-6 course_header_col c-left">
                <h5>
                    <a href="<?php echo site_url(); ?>">
                        <img src="<?php echo base_url().'uploads/system/logo-light-sm.png';?>" height="25"> |
                    </a>
                    <?php echo $course_details['title']; ?>
                </h5>
            </div>
            <div class="col-lg-6 course_header_col c-right">
                <a href="javascript::" class="course_btn toggle-lesson-view" onclick="toggle_lesson_view()"><i class="fa fa-arrows-alt-h"></i></a>
                <a href="<?php echo $my_course_url; ?>" class="course_btn my-course"> <i class="fa fa-chevron-left"></i> <?php echo get_phrase('my_courses'); ?></a>
                <a href="<?php echo $course_details_url; ?>" class="course_btn course-details"><?php echo get_phrase('course_details'); ?> <i class="fa fa-chevron-right"></i></a>
            </div>
        </div>
    </div>
  

    <div class="row" id = "lesson-container">
        <?php if (isset($lesson_id)): ?>
            <!-- Course content, video, quizes, files starts-->
            <?php include 'course_content_body.php'; ?>
            <!-- Course content, video, quizes, files ends-->
        <?php endif; ?>

        <!-- Course sections and lesson selector sidebar starts-->
        <?php include 'course_content_sidebar.php'; ?>
        <!-- Course sections and lesson selector sidebar ends-->
    </div>
</div>
