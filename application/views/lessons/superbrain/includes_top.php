<?php $version = microtime(true); ?>
<link rel="favicon" href="<?php echo base_url().'assets/frontend/superbrain/img/icons/favicon.ico' ?>">
<link rel="apple-touch-icon" href="<?php echo base_url().'assets/frontend/superbrain/img/icons/icon.png'; ?>">
<link rel="stylesheet" href="<?php echo base_url().'assets/frontend/superbrain/css/jquery.webui-popover.min.css'; ?>">
<link rel="stylesheet" href="<?php echo base_url().'assets/frontend/superbrain/css/select2.min.css'; ?>">
<link rel="stylesheet" href="<?php echo base_url().'assets/frontend/superbrain/css/slick.css'; ?>">
<link rel="stylesheet" href="<?php echo base_url().'assets/frontend/superbrain/css/slick-theme.css'; ?>">
<!-- font awesome 5 -->
<link rel="stylesheet" href="<?php echo base_url().'assets/frontend/superbrain/css/fontawesome-all.min.css'; ?>">

<link rel="stylesheet" href="<?php echo base_url().'assets/frontend/superbrain/css/bootstrap.min.css'; ?>">
<link rel="stylesheet" href="<?php echo base_url().'assets/frontend/superbrain/css/bootstrap-tagsinput.css'; ?>">
<link rel="stylesheet" href="<?php echo base_url().'assets/frontend/superbrain/css/main.css'; ?>">
<link rel="stylesheet" href="<?php echo base_url().'assets/frontend/superbrain/css/responsive.css'; ?>">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url().'assets/global/toastr/toastr.css' ?>">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/nestable2/1.6.0/jquery.nestable.min.css" />
<!-- <link rel="stylesheet" href="<?php echo base_url().'assets/lessons/css/custom.css' ; ?>"> -->
<link rel="stylesheet" href="<?php echo base_url().'assets/lessons/superbrain/css/custom.css?ver=' . $version ; ?>">
<link rel="stylesheet" href="<?php echo base_url().'assets/lessons/superbrain/css/responsive.css?ver=' . $version ; ?>">

<script src="<?php echo base_url('assets/backend/js/jquery-3.3.1.min.js'); ?>"></script>


<!-- Lesson page specific styles are here -->
<style type="text/css">

</style>
